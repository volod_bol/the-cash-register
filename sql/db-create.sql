-- Database: cashregister

DROP DATABASE IF EXISTS cashregister;

-- Create database via pgAdmin 4, psql create database with wrong encoding

CREATE DATABASE cashregister
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'C'
    LC_CTYPE = 'C'
    TABLESPACE = pg_default
	TEMPLATE = template0
    CONNECTION LIMIT = -1;

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    login varchar(20) UNIQUE NOT NULL,
	password varchar(60) NOT NULL,
	role VARCHAR(20) NOT NULL
);

CREATE TABLE goods (
	id SERIAL PRIMARY KEY,
	title VARCHAR(100) UNIQUE NOT NULL,
	quantity NUMERIC(9,2) NOT NULL
);

CREATE TABLE orders (
	id SERIAL PRIMARY KEY,
	user_login VARCHAR(20) REFERENCES users (login),
	date DATE NOT NULL DEFAULT CURRENT_DATE,
	time TIME NOT NULL DEFAULT LOCALTIME
);

CREATE TABLE orders_detail (
	orders_id INTEGER REFERENCES orders (id) ON DELETE CASCADE,
	goods_id INTEGER REFERENCES goods (id),
	goods_amount NUMERIC(9,2) NOT NULL
);

CREATE TABLE report_dates (
	id SERIAL PRIMARY KEY,
	last_orders_id INTEGER REFERENCES orders (id),
	type_of_report CHAR(1) DEFAULT 'X',
	user_login VARCHAR(20) REFERENCES users (login),
	date DATE NOT NULL DEFAULT CURRENT_DATE,
	time TIME NOT NULL DEFAULT LOCALTIME
);

-- USERS
-- commented queryes include decrypted password for authorization

INSERT INTO users (id, login, password, role) VALUES 
(DEFAULT, 'test', 'test', 'UNKNOWN');

-- INSERT INTO users (id, login, password, role) VALUES 
-- (DEFAULT, 'volod', 'xs77W4', 'SENIOR_CASHIER');
INSERT INTO users (id, login, password, role) VALUES 
(DEFAULT, 'volod', '4C1E394134A7F0BF18A3580CD39D850A', 'SENIOR_CASHIER');

-- INSERT INTO users (id, login, password, role) VALUES 
-- (DEFAULT, 'galiya', '483652fF', 'CASHIER');
INSERT INTO users (id, login, password, role) VALUES 
(DEFAULT, 'galiya', '5843D6776FE7617AFD91BB00A22BD009', 'CASHIER');

-- INSERT INTO users (id, login, password, role) VALUES 
-- (DEFAULT, 'oleksandrivna', 'gFjsk77', 'CASHIER');
INSERT INTO users (id, login, password, role) VALUES 
(DEFAULT, 'oleksandrivna', 'D5C19A7F468E7E46B65F7185FA03E8E8', 'CASHIER');

-- INSERT INTO users (id, login, password, role) VALUES 
-- (DEFAULT, 'valentina', 'xcnGG24', 'STOREKEEPER');
INSERT INTO users (id, login, password, role) VALUES 
(DEFAULT, 'valentina', 'CF509629713BB5A547DEECDDFD09EC57', 'STOREKEEPER');

-- Queue for rigth report function

INSERT INTO orders (id, user_login, date, time) VALUES
(DEFAULT, 'test', DEFAULT, DEFAULT);

INSERT INTO report_dates (id, last_orders_id, type_of_report, user_login, date, time) VALUES
(DEFAULT, 1, 'Z', 'test', DEFAULT, DEFAULT);

-- Queue with goods for warehouse

INSERT INTO goods (id, title, quantity) VALUES 
(DEFAULT, 'Кола 1.5', 100),
(DEFAULT, 'Газована вода 1.5', 100),
(DEFAULT, 'Хліб', 25),
(DEFAULT, 'Булочка з маком', 57),
(DEFAULT, 'Мачки пасла', 43),
(DEFAULT, 'Снікерс', 87),
(DEFAULT, 'Баунті', 143),
(DEFAULT, 'Піца', 5),
(DEFAULT, 'Антисептик', 366),
(DEFAULT, 'Маска медична', 12225),
(DEFAULT, 'Плейстешн 5', 6322),
(DEFAULT, 'Упаковка води 5л', 100),
(DEFAULT, 'Макбук М1', 2),
(DEFAULT, 'Солодощі', 3563),
(DEFAULT, 'Папір', 8796),
(DEFAULT, 'Портативна колонка', 345),
(DEFAULT, 'Монітор', 1235),
(DEFAULT, 'Ручка', 135),
(DEFAULT, 'Фонарик', 123),
(DEFAULT, 'Телефон Fly', 47),
(DEFAULT, 'Натс', 216),
(DEFAULT, 'Абонемент в спортзал', 125),
(DEFAULT, 'Компютерна миша', 366),
(DEFAULT, 'Кабель Type-C', 5212),
(DEFAULT, 'Айфон 4', 2435),
(DEFAULT, 'Сяомі Нот Супер Ультра 10 Макс', 439),
(DEFAULT, 'Пральна машина LG', 854),
(DEFAULT, 'Чіпси Lays', 6),
(DEFAULT, 'Морозиво', 55),
(DEFAULT, 'Банани', 45.75),
(DEFAULT, 'Куряча гомілка', 89.5),
(DEFAULT, 'Мюслі', 56),
(DEFAULT, 'RTX 3090', 234),
(DEFAULT, 'Плейсейшен 4', 2),
(DEFAULT, 'Останній XBOX', 80);
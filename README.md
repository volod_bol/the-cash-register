# the-cash-register

There are roles: cashier, senior cashier, storekeeper.

The storekeeper can create goods and indicate their quantity in the warehouse.

The cashier has the opportunity to:
- create an order (check);
- add selected products by their code or by product name to the order;
- specify / change the quantity of a certain product or weight;
- close the order (check);

The senior cashier has the opportunity to:
- cancel the check;
- cancel the goods in the receipt;
- make X and Z reports.

Technologies/Frameworks:
- 
* Java 11
* Java EE Servlet (Filters, Listeners)
* JSP
* JSTL
* JDBC
* Bootstrap
* log4j2
* Tomcat 9.0.58
* Maven
* Java EE Resource Bundle (i18n, i10n)
* JUnit
* Mockito
* RDBMS: PostgreSQL
* User passwords encrypted by AES256

Features
-
* i18n
* Pagination
* PRG pattern used
* Authentication and authorization
* Delimitation of authorities
* All inputs with data validation
* Error behavior (user don`t see server stack trace)
* Project build on MVC template
* Created own custom tag lib (user-friendly user role printer)

Participation:
-
- Developed whole application logic using servlets and JSP (user interaction and business logic) - Designed database schema and optimized queries to its
- Configured user authentication and authorization
- Formed secure credentials in a database (password encrypted by AES-256)
- Separated access to pages for each user role (using filters)
- Configured full warning/error logging by Log4j2
- Developed connection pool to the database
- Configured i18n (Ukrainian, English) by Java EE Resource Bundle (language changing provided by own written JavaScript function)
- Implemented SQL Transactions methods for a procession and confirming orders
- Designed JSP pages using Bootstrap 5 for a maximum user-friendly view
- Designed JSP pages with JSTL for displaying information from the database (using loops and other features from the template engine)
- Created own custom tag lib for JSP
- Involved algorithm for pagination without using additional frameworks (application get the only necessary information for page from database)
- Developed exception handling (user doesn't see server stack trace)
- Realized PRG design pattern for HTTP POST methods
- Provided fields validation on the backend side
- Covered whole business logic and 50% of project code by tests


Database diagram:
-
![db](servlet_cashregister-db.png)

First run
-
Create DB and insert data into by sql/db-create.sql script (basic users, goods for warehouse and test order for right report creating)
After that open src/main/webapp/WEB-INF/classes/app.properties and change connecting db link, username, and password
```
connection.url=jdbc:postgresql://localhost/cashregister?characterEncoding=utf8
user=postgres
password=root
```
In src/main/webapp/WEB-INF/log4j2.xml change APP_LOG_ROOT path for logs
```
<Properties>
    <Property name="LOG_PATTERN">%d{yyyy-MM-dd 'T'HH:mm:ss.SSSZ} %p %m%n</Property>
    <Property name="APP_LOG_ROOT">C:\Users\volod\Desktop\theCashRegister\cashregister\logs
    </Property>
</Properties>
```

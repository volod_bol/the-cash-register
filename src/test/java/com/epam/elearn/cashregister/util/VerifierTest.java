package com.epam.elearn.cashregister.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class VerifierTest {

    private Verifier verifier;

    @BeforeEach
    void setUp() {
        verifier = new Verifier();
    }

    @Test
    void constructor_throw_exception() {
        assertThrows(IllegalStateException.class, Verifier::new);
    }

    @ParameterizedTest
    @ValueSource(strings = {"-5", " ", "", "ddg", "0"})
    void order_with_wrong_id(String id) {
        assertFalse(verifier.isOrderInputCorrect("id", id, "30"));
    }

    @Test
    void order_with_wrong_amount() {
        assertFalse(verifier.isOrderInputCorrect("id", "7", "-123.6"));
    }

    @Test
    void order_with_null_amount() {
        assertFalse(verifier.isOrderInputCorrect("title", "7", null));
    }

    @Test
    void goods_order_blank_title() {
        assertFalse(verifier.isGoodsOrderCorrect("", "5"));
    }

    @Test
    void goods_order_null_quantity() {
        assertFalse(verifier.isGoodsOrderCorrect("Lay`s", null));
    }

    @Test
    void goods_order_null_title() {
        assertFalse(verifier.isGoodsOrderCorrect(null, "26.3"));
    }

    @Test
    void goods_order_wrong_quantity() {
        assertFalse(verifier.isGoodsOrderCorrect("Lay`s", "-23.5"));
    }

    @Test
    void goods_order_right_input() {
        assertTrue(verifier.isGoodsOrderCorrect("Lay`s", "0,2536"));
    }

    @Test
    void id_input_zero() {
        assertFalse(verifier.isIdInputCorrect("0"));
    }

    @Test
    void id_input_less_zero() {
        assertFalse(verifier.isIdInputCorrect("-32"));
    }

    @Test
    void id_input_decimal() {
        assertFalse(verifier.isIdInputCorrect("32.6"));
    }

    @Test
    void id_input_text() {
        assertFalse(verifier.isIdInputCorrect("Baunty"));
    }

    @Test
    void id_input_null() {
        assertFalse(verifier.isIdInputCorrect(null));
    }

    @Test
    void delete_goods_input_id_less_zero() {
        assertFalse(verifier.isDeleteGoodsOrderInputCorrect("-3", "0"));
    }

    @Test
    void delete_goods_input_id_zero() {
        assertFalse(verifier.isDeleteGoodsOrderInputCorrect("0", "0"));
    }

    @Test
    void delete_goods_input_id_text() {
        assertFalse(verifier.isDeleteGoodsOrderInputCorrect("tegs", "id"));
    }

    @Test
    void delete_goods_input_id_null() {
        assertFalse(verifier.isDeleteGoodsOrderInputCorrect(null, null));
    }

    @Test
    void delete_goods_right_input() {
        assertTrue(verifier.isDeleteGoodsOrderInputCorrect("5", "54"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"volod", "valentina", "galiya", "eugene_sumsky"})
    void correct_logins_test(String login) {
        assertTrue(verifier.isLoginCorrect(login));
    }

    @ParameterizedTest
    @ValueSource(strings = {"test", "matilda^", "termi=nator", ""})
    void incorrect_logins_test(String login) {
        assertFalse(verifier.isLoginCorrect(login));
    }

    @ParameterizedTest
    @ValueSource(strings = {"xs77W4", "483652fF^", "gFjsk77"})
    void correct_password_test(String password) {
        assertTrue(verifier.isPasswordCorrect(password));
    }

    @ParameterizedTest
    @ValueSource(strings = {"dg;564hh", "sjbcfgs", "57208757", ""})
    void incorrect_password_test(String password) {
        assertFalse(verifier.isPasswordCorrect(password));
    }

    @ParameterizedTest
    @ValueSource(strings = {"32", "634", "32.5", "397,77"})
    void correct_number_test(String number) {
        assertTrue(verifier.isNumberCorrect(number));
    }

    @ParameterizedTest
    @ValueSource(strings = {"-5", "-34.6", "-32,5", "what"})
    void incorrect_number_test(String number) {
        assertFalse(verifier.isNumberCorrect(number));
    }
}
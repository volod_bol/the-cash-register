package com.epam.elearn.cashregister.util;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.*;

class PasswordEncryptorTest {

    private static String key;
    private static String password = "oghfn5FF";
    private static String encryptPassword;

    @BeforeAll
    static void setUp() throws NoSuchAlgorithmException {
        key = PasswordEncryptor.KeyUtils.generateKey();
        encryptPassword = new PasswordEncryptor(key).encryptPassword(password);
    }

    @Test
    void object_instance_outside_servlet() {
        assertThrows(ExceptionInInitializerError.class, () -> PasswordEncryptor.getInstance());
    }

    @Test
    void original_and_encrypted_passwords_different() {
        assertNotEquals(password, encryptPassword);
    }

    @Test
    void original_and_decrypted_passwords_equal() {
        String decryptPassword = new PasswordEncryptor(key).decryptPassword(encryptPassword);
        assertEquals(password, decryptPassword);
    }

    @Test
    void different_keys_can_not_encrypt_and_decrypt_password() throws NoSuchAlgorithmException {
        String key = PasswordEncryptor.KeyUtils.generateKey();
        String password = "gdfstYYR78";
        String encryptPassword = new PasswordEncryptor(key).encryptPassword(password);
        key = PasswordEncryptor.KeyUtils.generateKey();
        String decryptPassword = new PasswordEncryptor(key).decryptPassword(encryptPassword);
        assertNotEquals(password, decryptPassword);
    }
}
package com.epam.elearn.cashregister.util;

import com.epam.elearn.cashregister.entity.GoodsOrder;
import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

class ServletHelperTest {

    @Test
    void constructor_throw_illegal_state_exp() {
        assertThrows(IllegalStateException.class, ServletHelper::new);
    }

    @Test
    void clean_orders_four_times() {
        ServletHelper servletHelper = new ServletHelper();
        HttpSession session = mock(HttpSession.class);
        servletHelper.removeOrderInfo(session);
        verify(session, times(4)).removeAttribute(anyString());
    }

    @Test
    void set_orders_from_session_to_request_order_list_exist() {
        ServletHelper servletHelper = new ServletHelper();
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpSession session = mock(HttpSession.class);
        List<GoodsOrder> goodsOrderList = new ArrayList<>();
        String listAttribute = "list";
        when(request.getSession()).thenReturn(session);
        when(request.getSession().getAttribute(listAttribute)).thenReturn(goodsOrderList);
        servletHelper.setOrdersFromSessionToRequest(request, listAttribute);
        verify(request).setAttribute(listAttribute, goodsOrderList);
    }

    @Test
    void set_orders_from_session_to_request_order_list_null() {
        ServletHelper servletHelper = new ServletHelper();
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpSession session = mock(HttpSession.class);
        String listAttribute = "list";
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(listAttribute)).thenReturn(null);
        servletHelper.setOrdersFromSessionToRequest(request, listAttribute);
        verify(session).setAttribute(eq(listAttribute), anyList());
        verify(request).setAttribute(eq(listAttribute), anyList());
    }

    @Test
    void mark_wrong_input() {
        ServletHelper servletHelper = new ServletHelper();
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpSession session = mock(HttpSession.class);
        String wrongInput = "wrongInput";
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(wrongInput)).thenReturn(true);
        servletHelper.markIfWrongInput(request, wrongInput);
        verify(request).setAttribute(eq(wrongInput), any());
        verify(session).removeAttribute(wrongInput);
    }
}
package com.epam.elearn.cashregister.database;

import com.epam.elearn.cashregister.entity.*;
import org.junit.jupiter.api.Test;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static com.epam.elearn.cashregister.database.DBManager.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DBManagerTest {

    @Test
    void constructor_throw_illegal_state_exp() {
        assertThrows(IllegalStateException.class, DBManager::new);
    }

    @Test
    void get_user_in_db_exist() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement statement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(statement);
        when(statement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(true);
        when(resultSet.getInt("id")).thenReturn(2);
        when(resultSet.getString("password")).thenReturn("password123");
        when(resultSet.getString("role")).thenReturn("SENIOR_CASHIER");

        assertEquals(new User(2, "volod", "password123", Role.SENIOR_CASHIER), getUser(connection, "volod"));
    }

    @Test
    void get_user_in_db_non_exist() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement statement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(statement);
        when(statement.executeQuery()).thenReturn(resultSet);

        assertNull(getUser(connection, "volod"));
    }

    @Test
    void get_user_in_db_exp() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement statement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(statement);
        when(statement.executeQuery()).thenThrow(SQLException.class);

        assertNull(getUser(connection, "volod"));
    }

    @Test
    void add_user_to_db() throws SQLException {
        User user = new User(8, "toreto", "trhdodfp4383", Role.CASHIER);

        Connection connection = mock(Connection.class);
        PreparedStatement statement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(statement);
        when(statement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(true);
        when(resultSet.getInt("id")).thenReturn(8);

        assertEquals(user, addUser(connection, user));
    }

    @Test
    void add_user_to_db_exp() throws SQLException {
        User user = new User(0, "toreto", "trhdodfp4383", Role.CASHIER);

        Connection connection = mock(Connection.class);
        PreparedStatement statement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(statement);
        when(statement.executeQuery()).thenThrow(SQLException.class);

        assertEquals(user, addUser(connection, user));
    }


    @Test
    void user_in_db_exist_and_login_already_used() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement statement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(statement);
        when(statement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(true);

        assertFalse(isUserLoginFree(connection, "volod"));
    }

    @Test
    void user_in_db_non_exist_and_login_free() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement statement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(statement);
        when(statement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(false);

        assertTrue(isUserLoginFree(connection, "volod"));
    }

    @Test
    void user_in_db_non_exist_and_login_free_exp() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement statement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(statement);
        when(statement.executeQuery()).thenThrow(SQLException.class);

        assertFalse(isUserLoginFree(connection, "volod"));
    }

    @Test
    void get_count_of_goods_rs_next_true() throws SQLException {
        Connection connection = mock(Connection.class);
        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);
        ResultSet resultSet = mock(ResultSet.class);
        when(statement.executeQuery(anyString())).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(true);
        when(resultSet.getInt("count")).thenReturn(23);

        assertEquals(23, getCountOfGoods(connection));
    }

    @Test
    void get_count_of_goods_rs_next_false() throws SQLException {
        Connection connection = mock(Connection.class);
        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);
        ResultSet resultSet = mock(ResultSet.class);
        when(statement.executeQuery(anyString())).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(false);
        when(resultSet.getInt("count")).thenReturn(23);

        assertEquals(0, getCountOfGoods(connection));
    }

    @Test
    void get_count_of_goods_exp() throws SQLException {
        Connection connection = mock(Connection.class);
        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);
        when(statement.executeQuery(anyString())).thenThrow(SQLException.class);

        assertEquals(0, getCountOfGoods(connection));
    }


    @Test
    void insert_into_goods_true() throws SQLException {
        Goods goods = new Goods(4, "Snickers", 5373);
        Connection connection = mock(Connection.class);
        PreparedStatement statement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(statement);
        ResultSet resultSet = mock(ResultSet.class);
        when(statement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(true);
        when(resultSet.getInt("id")).thenReturn(4);

        assertEquals(goods, insertIntoGoods(connection, goods.getTitle(), goods.getQuantity()));
    }

    @Test
    void insert_into_goods_false() throws SQLException {
        Goods goods = new Goods(4, "Snickers", 5373);
        Connection connection = mock(Connection.class);
        PreparedStatement statement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(statement);
        ResultSet resultSet = mock(ResultSet.class);
        when(statement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(false);

        assertNull(insertIntoGoods(connection, goods.getTitle(), goods.getQuantity()));
    }

    @Test
    void insert_into_goods_exp() throws SQLException {
        Goods goods = new Goods(0, "Snickers", 5373);
        Connection connection = mock(Connection.class);
        PreparedStatement statement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(statement);
        when(statement.executeQuery()).thenThrow(SQLException.class);

        assertNull(insertIntoGoods(connection, goods.getTitle(), goods.getQuantity()));
    }

    @Test
    void update_goods_quantity_id_lt_count() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(1);

        Statement statement = mock(Statement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.createStatement()).thenReturn(statement);
        when(statement.executeQuery(anyString())).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true);
        when(resultSet.getInt("count")).thenReturn(35);

        assertTrue(updateGoodsQuantity(connection, 14, 15.65));
    }

    @Test
    void update_goods_quantity_id_gt_count() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(1);

        Statement statement = mock(Statement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.createStatement()).thenReturn(statement);
        when(statement.executeQuery(anyString())).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true);
        when(resultSet.getInt("count")).thenReturn(12);

        assertFalse(updateGoodsQuantity(connection, 14, 15.65));
    }

    @Test
    void update_goods_quantity_exp() throws SQLException {
        Connection connection = mock(Connection.class);
        when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);

        Statement statement = mock(Statement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.createStatement()).thenReturn(statement);
        when(statement.executeQuery(anyString())).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true);
        when(resultSet.getInt("count")).thenReturn(16);

        assertFalse(updateGoodsQuantity(connection, 14, 15.65));
    }

    @Test
    void get_count_of_orders_rs_next_true() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(true);
        when(resultSet.getInt("count")).thenReturn(65);

        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);

        when(statement.executeQuery(anyString())).thenReturn(resultSet);
        when(resultSet.getInt("last_orders_id")).thenReturn(5);

        assertEquals(65, getCountOfOrders(connection));
    }

    @Test
    void get_count_of_orders_exp() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenThrow(SQLException.class);

        when(resultSet.next()).thenReturn(true);
        when(resultSet.getInt("count")).thenReturn(65);

        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);

        when(statement.executeQuery(anyString())).thenReturn(resultSet);
        when(resultSet.getInt("last_orders_id")).thenReturn(5);

        assertEquals(0, getCountOfOrders(connection));
    }

    @Test
    void get_all_goods_no_goods() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        List<Goods> goods = getAllGoods(connection, 0, 20);

        assertEquals(0, goods.size());
    }

    @Test
    void get_all_goods_exp() throws SQLException {
        Connection connection = mock(Connection.class);
        when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);

        List<Goods> goods = getAllGoods(connection, 0, 20);

        assertEquals(0, goods.size());
    }

    @Test
    void get_all_goods() throws SQLException {
        List<Goods> goods = new ArrayList<>();
        goods.add(new Goods(4, "Snickers", 345.0));
        goods.add(new Goods(8, "���", 93.0));
        goods.add(new Goods(2, "����", 53.0));
        goods.add(new Goods(5, "Lenovo", 3.0));
        goods.add(new Goods(7, "Xbox", 4.0));

        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(true, true, true, true, true, false);
        when(resultSet.getInt("id")).thenReturn(4, 8, 2, 5, 7);
        when(resultSet.getString("title")).thenReturn("Snickers", "���", "����", "Lenovo", "Xbox");
        when(resultSet.getDouble("quantity")).thenReturn(345.0, 93.0, 53.0, 3.0, 4.0);

        assertTrue(getAllGoods(connection, 0, 10).containsAll(goods));
    }

    @Test
    void get_count_of_orders_rs_next_false() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(false);
        when(resultSet.getInt("count")).thenReturn(65);

        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);

        when(statement.executeQuery(anyString())).thenReturn(resultSet);
        when(resultSet.getInt("last_orders_id")).thenReturn(5);

        assertEquals(0, getCountOfOrders(connection));
    }

    @Test
    void get_order_exist() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        int id = 525;
        Date date = Date.valueOf(LocalDate.now());
        Time time = Time.valueOf(LocalTime.now());

        when(resultSet.next()).thenReturn(true);
        when(resultSet.getString("user_login")).thenReturn("volod");
        when(resultSet.getDate(anyString())).thenReturn(date);
        when(resultSet.getTime(anyString())).thenReturn(time);

        Order order = new Order(id, "volod", new ArrayList<>(), date.toLocalDate(), time.toLocalTime());

        assertEquals(order, getOrder(connection, id));
    }

    @Test
    void get_order_non_exist() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(false);

        assertNull(getOrder(connection, 4646));
    }

    @Test
    void get_order_exp() throws SQLException {
        Connection connection = mock(Connection.class);
        when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);

        assertNull(getOrder(connection, 4646));
    }

    @Test
    void get_orders_false() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);
        when(statement.executeQuery(anyString())).thenReturn(resultSet);

        assertEquals(0, getAllOrders(connection).size());
    }

    @Test
    void get_orders_exp() throws SQLException {
        Connection connection = mock(Connection.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);

        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);
        when(statement.executeQuery(anyString())).thenReturn(resultSet);

        assertEquals(0, getAllOrders(connection).size());
    }

    @Test
    void get_orders() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet preparedResultSet = mock(ResultSet.class);
        when(connection.prepareStatement(SQLQuery.GET_ALL_ORDERS.query)).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(preparedResultSet);
        when(preparedResultSet.next()).thenReturn(true, true, false);

        Statement statement = mock(Statement.class);
        ResultSet statementResultSet = mock(ResultSet.class);
        when(connection.createStatement()).thenReturn(statement);
        when(statement.executeQuery("SELECT last_orders_id FROM report_dates WHERE type_of_report = 'Z' ORDER BY id DESC LIMIT 1")).thenReturn(statementResultSet);
        when(statementResultSet.next()).thenReturn(true);
        when(statementResultSet.getInt("last_orders_id")).thenReturn(6);

        PreparedStatement goodsInOrderStatement = mock(PreparedStatement.class);
        ResultSet goodsInOrderRSet = mock(ResultSet.class);
        when(connection.prepareStatement(SQLQuery.GET_ALL_GOODS_IN_ORDER.query)).thenReturn(goodsInOrderStatement);
        when(goodsInOrderStatement.executeQuery()).thenReturn(goodsInOrderRSet);
        when(goodsInOrderRSet.next()).thenReturn(true, true, false);
        when(goodsInOrderRSet.getInt("goods_id")).thenReturn(5, 2);
        when(goodsInOrderRSet.getString("title")).thenReturn("Baunti", "PlayStation");
        when(goodsInOrderRSet.getDouble("goods_amount")).thenReturn(44.0, 7.0);

        List<GoodsOrder> goods = List.of(new GoodsOrder(5, "Baunti", "44"), new GoodsOrder(2, "PlayStation", "7"));

        Date date = Date.valueOf(LocalDate.now());
        Time time = Time.valueOf(LocalTime.now());
        when(preparedResultSet.getInt("id")).thenReturn(65, 44);
        when(preparedResultSet.getString("user_login")).thenReturn("valentina", "oleksandrivna");
        when(preparedResultSet.getDate("date")).thenReturn(date, date);
        when(preparedResultSet.getTime("time")).thenReturn(time, time);
        List<Order> orders = List.of(
                new Order(65, "valentina", goods, date.toLocalDate(), time.toLocalTime()),
                new Order(44, "oleksandrivna", goods, date.toLocalDate(), time.toLocalTime())
        );

        List<Order> allOrders = getAllOrders(connection);

        assertTrue(allOrders.containsAll(orders));
        assertEquals(allOrders.get(0), orders.get(0));
        assertEquals(allOrders.get(1), orders.get(1));
    }

    @Test
    void get_orders_pagination() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet preparedResultSet = mock(ResultSet.class);
        when(connection.prepareStatement(SQLQuery.GET_ALL_ORDERS_PAGINATION.query)).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(preparedResultSet);
        when(preparedResultSet.next()).thenReturn(true, true, false);

        Statement statement = mock(Statement.class);
        ResultSet statementResultSet = mock(ResultSet.class);
        when(connection.createStatement()).thenReturn(statement);
        when(statement.executeQuery("SELECT last_orders_id FROM report_dates WHERE type_of_report = 'Z' ORDER BY id DESC LIMIT 1")).thenReturn(statementResultSet);
        when(statementResultSet.next()).thenReturn(true);
        when(statementResultSet.getInt("last_orders_id")).thenReturn(6);

        PreparedStatement goodsInOrderStatement = mock(PreparedStatement.class);
        ResultSet goodsInOrderRSet = mock(ResultSet.class);
        when(connection.prepareStatement(SQLQuery.GET_ALL_GOODS_IN_ORDER.query)).thenReturn(goodsInOrderStatement);
        when(goodsInOrderStatement.executeQuery()).thenReturn(goodsInOrderRSet);
        when(goodsInOrderRSet.next()).thenReturn(true, true, false);
        when(goodsInOrderRSet.getInt("goods_id")).thenReturn(5, 2);
        when(goodsInOrderRSet.getString("title")).thenReturn("Baunti", "PlayStation");
        when(goodsInOrderRSet.getDouble("goods_amount")).thenReturn(44.0, 7.0);

        List<GoodsOrder> goods = List.of(new GoodsOrder(5, "Baunti", "44"), new GoodsOrder(2, "PlayStation", "7"));

        Date date = Date.valueOf(LocalDate.now());
        Time time = Time.valueOf(LocalTime.now());
        when(preparedResultSet.getInt("id")).thenReturn(65, 44);
        when(preparedResultSet.getString("user_login")).thenReturn("valentina", "oleksandrivna");
        when(preparedResultSet.getDate("date")).thenReturn(date, date);
        when(preparedResultSet.getTime("time")).thenReturn(time, time);
        List<Order> orders = List.of(
                new Order(65, "valentina", goods, date.toLocalDate(), time.toLocalTime()),
                new Order(44, "oleksandrivna", goods, date.toLocalDate(), time.toLocalTime())
        );

        List<Order> allOrders = getAllOrders(connection, 10, 5);

        assertTrue(allOrders.containsAll(orders));
        assertEquals(allOrders.get(0), orders.get(0));
        assertEquals(allOrders.get(1), orders.get(1));
    }

    @Test
    void get_orders_pagination_exp() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet preparedResultSet = mock(ResultSet.class);
        when(connection.prepareStatement(SQLQuery.GET_ALL_ORDERS_PAGINATION.query)).thenThrow(SQLException.class);
        when(preparedStatement.executeQuery()).thenReturn(preparedResultSet);
        when(preparedResultSet.next()).thenReturn(true, true, false);

        Statement statement = mock(Statement.class);
        ResultSet statementResultSet = mock(ResultSet.class);
        when(connection.createStatement()).thenReturn(statement);
        when(statement.executeQuery("SELECT last_orders_id FROM report_dates WHERE type_of_report = 'Z' ORDER BY id DESC LIMIT 1")).thenReturn(statementResultSet);
        when(statementResultSet.next()).thenReturn(true);
        when(statementResultSet.getInt("last_orders_id")).thenReturn(6);

        PreparedStatement goodsInOrderStatement = mock(PreparedStatement.class);
        ResultSet goodsInOrderRSet = mock(ResultSet.class);
        when(connection.prepareStatement(SQLQuery.GET_ALL_GOODS_IN_ORDER.query)).thenReturn(goodsInOrderStatement);
        when(goodsInOrderStatement.executeQuery()).thenReturn(goodsInOrderRSet);
        when(goodsInOrderRSet.next()).thenReturn(true, true, false);
        when(goodsInOrderRSet.getInt("goods_id")).thenReturn(5, 2);
        when(goodsInOrderRSet.getString("title")).thenReturn("Baunti", "PlayStation");
        when(goodsInOrderRSet.getDouble("goods_amount")).thenReturn(44.0, 7.0);

        Date date = Date.valueOf(LocalDate.now());
        Time time = Time.valueOf(LocalTime.now());
        when(preparedResultSet.getInt("id")).thenReturn(65, 44);
        when(preparedResultSet.getString("user_login")).thenReturn("valentina", "oleksandrivna");
        when(preparedResultSet.getDate("date")).thenReturn(date, date);
        when(preparedResultSet.getTime("time")).thenReturn(time, time);

        assertTrue(getAllOrders(connection, 10, 5).isEmpty());
    }

    @Test
    void delete_order_non_exist() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet preparedResultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(preparedResultSet);

        assertNull(deleteOrder(connection, 5));
    }

    @Test
    void delete_order_exp() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet preparedResultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(preparedResultSet);
        when(preparedResultSet.next()).thenReturn(true);

        Date date = Date.valueOf(LocalDate.now());
        Time time = Time.valueOf(LocalTime.now());
        when(preparedResultSet.getString("user_login")).thenReturn("oleksandrivna");
        when(preparedResultSet.getDate("date")).thenReturn(date);
        when(preparedResultSet.getTime("time")).thenReturn(time);

        when(preparedStatement.executeUpdate()).thenThrow(SQLException.class);

        assertNull(deleteOrder(connection, 5));
    }

    @Test
    void delete_order_exist() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet preparedResultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(preparedResultSet);
        when(preparedResultSet.next()).thenReturn(true);

        Date date = Date.valueOf(LocalDate.now());
        Time time = Time.valueOf(LocalTime.now());
        when(preparedResultSet.getString("user_login")).thenReturn("oleksandrivna");
        when(preparedResultSet.getDate("date")).thenReturn(date);
        when(preparedResultSet.getTime("time")).thenReturn(time);
        Order order = new Order(5, "oleksandrivna", new ArrayList<>(), date.toLocalDate(), time.toLocalTime());

        assertEquals(order, deleteOrder(connection, 5));
    }

    @Test
    void delete_goods_in_order_non_exist() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet preparedResultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(preparedResultSet);

        assertNull(deleteGoodsInOrder(connection, 8, 16));
    }

    @Test
    void delete_goods_in_order_exp() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenThrow(SQLException.class);

        assertNull(deleteGoodsInOrder(connection, 8, 16));
    }

    @Test
    void delete_goods_in_order_exist() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet preparedResultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(preparedResultSet);
        when(preparedResultSet.next()).thenReturn(true);
        when(preparedResultSet.getDouble("goods_amount")).thenReturn(7.0);

        PreparedStatement goodsInOrderStatement = mock(PreparedStatement.class);
        ResultSet goodsInOrderRSet = mock(ResultSet.class);
        when(connection.prepareStatement(SQLQuery.GET_ALL_GOODS_IN_ORDER.query)).thenReturn(goodsInOrderStatement);
        when(goodsInOrderStatement.executeQuery()).thenReturn(goodsInOrderRSet);
        when(goodsInOrderRSet.next()).thenReturn(true, true, false);
        when(goodsInOrderRSet.getInt("goods_id")).thenReturn(5, 2);
        when(goodsInOrderRSet.getString("title")).thenReturn("Baunti", "PlayStation");
        when(goodsInOrderRSet.getDouble("goods_amount")).thenReturn(44.0, 7.0);

        PreparedStatement goodsOrderByIdStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(SQLQuery.GET_GOODS_BY_ID.query)).thenReturn(goodsOrderByIdStatement);
        ResultSet goodsOrderByIdRSet = mock(ResultSet.class);
        when(goodsOrderByIdStatement.executeQuery()).thenReturn(goodsOrderByIdRSet);
        when(goodsOrderByIdRSet.next()).thenReturn(true);
        when(goodsOrderByIdRSet.getString("title")).thenReturn("PlayStation");

        GoodsOrder goodsOrder = new GoodsOrder(2, "PlayStation", "7.0");

        assertEquals(goodsOrder, deleteGoodsInOrder(connection, 8, 2));
    }

    @Test
    void delete_goods_in_order_was_last_goods() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet preparedResultSet = mock(ResultSet.class);
        when(connection.prepareStatement(SQLQuery.DELETE_GOODS_IN_ORDER.query)).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(preparedResultSet);
        when(preparedResultSet.next()).thenReturn(true);
        when(preparedResultSet.getDouble("goods_amount")).thenReturn(7.0);

        PreparedStatement goodsInOrderStatement = mock(PreparedStatement.class);
        ResultSet goodsInOrderRSet = mock(ResultSet.class);
        when(connection.prepareStatement(SQLQuery.GET_ALL_GOODS_IN_ORDER.query)).thenReturn(goodsInOrderStatement);
        when(goodsInOrderStatement.executeQuery()).thenReturn(goodsInOrderRSet);

        PreparedStatement orderByIdStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(SQLQuery.GET_ORDER_BY_ID.query)).thenReturn(orderByIdStatement);
        ResultSet orderByIdRSet = mock(ResultSet.class);
        when(orderByIdStatement.executeQuery()).thenReturn(orderByIdRSet);
        when(orderByIdRSet.next()).thenReturn(true);

        Date date = Date.valueOf(LocalDate.now());
        Time time = Time.valueOf(LocalTime.now());
        when(orderByIdRSet.getString("user_login")).thenReturn("oleksandrivna");
        when(orderByIdRSet.getDate("date")).thenReturn(date);
        when(orderByIdRSet.getTime("time")).thenReturn(time);

        PreparedStatement deleteOrderStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(SQLQuery.DELETE_ORDER.query)).thenReturn(deleteOrderStatement);

        PreparedStatement goodsOrderByIdStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(SQLQuery.GET_GOODS_BY_ID.query)).thenReturn(goodsOrderByIdStatement);
        ResultSet goodsOrderByIdRSet = mock(ResultSet.class);
        when(goodsOrderByIdStatement.executeQuery()).thenReturn(goodsOrderByIdRSet);
        when(goodsOrderByIdRSet.next()).thenReturn(true);
        when(goodsOrderByIdRSet.getString("title")).thenReturn("PlayStation");

        GoodsOrder goodsOrder = new GoodsOrder(2, "PlayStation", "7.0");

        assertEquals(goodsOrder, deleteGoodsInOrder(connection, 8, 2));

        verify(connection).prepareStatement(SQLQuery.GET_ORDER_BY_ID.query);
        verify(orderByIdStatement).executeQuery();
        verify(connection).prepareStatement(SQLQuery.DELETE_ORDER.query);
    }

    @Test
    void get_goods_from_order_by_id_exist() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        int id = 5;
        when(resultSet.next()).thenReturn(true);
        when(resultSet.getString("title")).thenReturn("Bananas");

        GoodsOrder goodsOrder = new GoodsOrder(id, "Bananas", "64.7");

        assertEquals(goodsOrder, getGoodsOrderById(connection, id, "64.7"));
    }

    @Test
    void get_goods_from_order_by_id_non_exist() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(false);

        assertNull(getGoodsOrderById(connection, 75, "64.7"));
    }

    @Test
    void get_goods_from_order_by_id_exp() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenThrow(SQLException.class);

        assertNull(getGoodsOrderById(connection, 75, "64.7"));
    }

    @Test
    void get_goods_from_order_by_title_exist() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        int id = 85;
        String title = "Bananas";
        String amount = "25.884";
        when(resultSet.next()).thenReturn(true);
        when(resultSet.getInt("id")).thenReturn(id);

        GoodsOrder goodsOrder = new GoodsOrder(id, title, amount);

        assertEquals(goodsOrder, getGoodsByTitle(connection, title, amount));
    }

    @Test
    void get_goods_from_order_by_title_non_exist() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        String title = "Bananas";
        String amount = "25.884";
        when(resultSet.next()).thenReturn(false);

        assertNull(getGoodsByTitle(connection, title, amount));
    }

    @Test
    void get_goods_from_order_by_title_exp() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenThrow(SQLException.class);

        String title = "Bananas";
        String amount = "25.884";

        assertNull(getGoodsByTitle(connection, title, amount));
    }

    @Test
    void get_all_goods_from_order_exp() throws SQLException {
        Connection connection = mock(Connection.class);
        when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);

        assertTrue(getAllGoodsFromOrder(connection, 65).isEmpty());
    }

    @Test
    void confirming_order_no_error() throws SQLException {
        Connection connection = mock(Connection.class);
        List<GoodsOrder> goodsOrderList = new ArrayList<>();
        goodsOrderList.add(new GoodsOrder(4, "Snickers", "345.0"));
        goodsOrderList.add(new GoodsOrder(8, "���", "93.0"));
        goodsOrderList.add(new GoodsOrder(2, "����", "53.0"));
        goodsOrderList.add(new GoodsOrder(5, "Lenovo", "3.0"));
        goodsOrderList.add(new GoodsOrder(7, "Xbox", "4.0"));
        String userLogin = "valentina";

        PreparedStatement insIntoOrder = mock(PreparedStatement.class);
        PreparedStatement instIntoOrderDetail = mock(PreparedStatement.class);
        when(connection.prepareStatement(SQLQuery.INSERT_INTO_ORDERS.query)).thenReturn(insIntoOrder);
        when(connection.prepareStatement(SQLQuery.INSERT_INTO_ORDERS_DETAIL.query)).thenReturn(instIntoOrderDetail);

        ResultSet insIntoOrderRSet = mock(ResultSet.class);
        when(insIntoOrderRSet.next()).thenReturn(true);
        when(insIntoOrderRSet.getInt("id")).thenReturn(74);
        when(insIntoOrder.executeQuery()).thenReturn(insIntoOrderRSet);

        assertTrue(confirmOrder(connection, goodsOrderList, userLogin));

        verify(instIntoOrderDetail, times(5)).executeUpdate();
        verify(connection).setAutoCommit(false);
        verify(connection).commit();
        verify(connection).setAutoCommit(true);
    }

    @Test
    void confirming_order_error() throws SQLException {
        Connection connection = mock(Connection.class);
        List<GoodsOrder> goodsOrderList = new ArrayList<>();
        goodsOrderList.add(new GoodsOrder(4, "Snickers", "345.0"));
        goodsOrderList.add(new GoodsOrder(8, "���", "93.0"));
        goodsOrderList.add(new GoodsOrder(2, "����", "53.0"));
        goodsOrderList.add(new GoodsOrder(5, "Lenovo", "3.0"));
        goodsOrderList.add(new GoodsOrder(7, "Xbox", "4.0"));
        String userLogin = "valentina";

        PreparedStatement insIntoOrder = mock(PreparedStatement.class);
        PreparedStatement instIntoOrderDetail = mock(PreparedStatement.class);
        when(connection.prepareStatement(SQLQuery.INSERT_INTO_ORDERS.query)).thenReturn(insIntoOrder);
        when(connection.prepareStatement(SQLQuery.INSERT_INTO_ORDERS_DETAIL.query)).thenReturn(instIntoOrderDetail);

        ResultSet insIntoOrderRSet = mock(ResultSet.class);
        when(insIntoOrderRSet.next()).thenReturn(true);
        when(insIntoOrderRSet.getInt("id")).thenReturn(74);
        when(insIntoOrder.executeQuery()).thenReturn(insIntoOrderRSet);

        when(instIntoOrderDetail.executeUpdate()).thenThrow(SQLException.class);

        assertFalse(confirmOrder(connection, goodsOrderList, userLogin));

        verify(instIntoOrderDetail).executeUpdate();
        verify(connection).setAutoCommit(false);
        verify(connection).rollback();
        verify(connection).setAutoCommit(true);
    }

    @Test
    void count_of_purchased_goods() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);
        when(statement.executeQuery(anyString())).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(true);
        when(resultSet.getInt("last_orders_id")).thenReturn(4);

        when(resultSet.getInt("sum")).thenReturn(4723);

        assertEquals(4723, getCountPurchasedGoods(connection));
    }

    @Test
    void count_of_purchased_goods_false() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(false);

        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);
        when(statement.executeQuery(anyString())).thenReturn(resultSet);

        assertEquals(0, getCountPurchasedGoods(connection));
    }

    @Test
    void count_of_purchased_goods_exp() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        assertEquals(0, getCountPurchasedGoods(connection));
    }

    @Test
    void confirm_x_report() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        ResultSet preparedRSet = mock(ResultSet.class);
        when(preparedStatement.executeQuery()).thenReturn(preparedRSet);

        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);
        ResultSet statementRSet = mock(ResultSet.class);
        when(statement.executeQuery(anyString())).thenReturn(statementRSet);
        when(statementRSet.next()).thenReturn(true);
        when(statementRSet.getInt("last_orders_id")).thenReturn(54);

        when(preparedRSet.next()).thenReturn(true);
        when(preparedRSet.getInt("id")).thenReturn(43);

        assertEquals(43, confirmXReport(connection, "valentina"));
    }

    @Test
    void confirm_x_report_error() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        ResultSet preparedRSet = mock(ResultSet.class);
        when(preparedStatement.executeQuery()).thenReturn(preparedRSet);

        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);
        ResultSet statementRSet = mock(ResultSet.class);
        when(statement.executeQuery(anyString())).thenReturn(statementRSet);
        when(statementRSet.next()).thenReturn(true);
        when(statementRSet.getInt("last_orders_id")).thenReturn(54);

        assertEquals(0, confirmXReport(connection, "valentina"));
    }

    @Test
    void confirm_x_report_exp() throws SQLException {
        Connection connection = mock(Connection.class);
        when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);

        assertEquals(0, confirmXReport(connection, "valentina"));
    }

    @Test
    void confirm_z_report() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        ResultSet preparedRSet = mock(ResultSet.class);
        when(preparedStatement.executeQuery()).thenReturn(preparedRSet);

        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);
        ResultSet statementRSet = mock(ResultSet.class);
        when(statement.executeQuery(anyString())).thenReturn(statementRSet);
        when(statementRSet.next()).thenReturn(true);
        when(statementRSet.getInt("last_orders_id")).thenReturn(77);

        when(preparedRSet.next()).thenReturn(true);
        when(preparedRSet.getInt("id")).thenReturn(46);

        assertEquals(46, confirmZReport(connection, "valentina"));
    }

    @Test
    void confirm_z_report_error() throws SQLException {
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        ResultSet preparedRSet = mock(ResultSet.class);
        when(preparedStatement.executeQuery()).thenReturn(preparedRSet);

        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);
        ResultSet statementRSet = mock(ResultSet.class);
        when(statement.executeQuery(anyString())).thenReturn(statementRSet);
        when(statementRSet.next()).thenReturn(true);
        when(statementRSet.getInt("last_orders_id")).thenReturn(77);

        assertEquals(0, confirmZReport(connection, "valentina"));
    }

    @Test
    void confirm_z_report_exp() throws SQLException {
        Connection connection = mock(Connection.class);
        when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);

        assertEquals(0, confirmZReport(connection, "valentina"));
    }

    @Test
    void get_last_order_id_order_exist() throws SQLException {
        Connection connection = mock(Connection.class);
        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);
        ResultSet resultSet = mock(ResultSet.class);
        when(statement.executeQuery(anyString())).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true);
        when(resultSet.getInt("id")).thenReturn(76);

        assertEquals(76, getLastOrderId(connection));
    }

    @Test
    void get_last_order_id_order_non_exist() throws SQLException {
        Connection connection = mock(Connection.class);
        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);
        ResultSet resultSet = mock(ResultSet.class);
        when(statement.executeQuery(anyString())).thenReturn(resultSet);

        assertEquals(0, getLastOrderId(connection));
    }

    @Test
    void get_last_order_id_exp() throws SQLException {
        Connection connection = mock(Connection.class);
        when(connection.createStatement()).thenThrow(SQLException.class);

        assertEquals(0, getLastOrderId(connection));
    }

    @Test
    void get_last_order_id_from_report_exp() throws SQLException {
        Connection connection = mock(Connection.class);
        when(connection.createStatement()).thenThrow(SQLException.class);

        assertEquals(0, getLastOrderReportId(connection));
    }

}
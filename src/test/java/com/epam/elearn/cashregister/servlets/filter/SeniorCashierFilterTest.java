package com.epam.elearn.cashregister.servlets.filter;

import com.epam.elearn.cashregister.entity.Role;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.mockito.Mockito.*;

class SeniorCashierFilterTest {

    private static SeniorCashierFilter filter;

    @BeforeAll
    static void objMapping() {
        filter = new SeniorCashierFilter();
    }

    @Test
    void right_role_access() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("role")).thenReturn(Role.SENIOR_CASHIER.name());

        filter.doFilter(request, response, filterChain);

        verify(filterChain, times(1)).doFilter(request, response);
    }

    @Test
    void wrong_role_access() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("role")).thenReturn(Role.STOREKEEPER.name());

        filter.doFilter(request, response, filterChain);

        verify(response, times(1)).sendError(401);
    }

    @Test
    void null_role_access() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("role")).thenReturn(null);

        filter.doFilter(request, response, filterChain);

        verify(response, times(1)).sendError(401);
    }

}
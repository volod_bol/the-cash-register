package com.epam.elearn.cashregister.servlets.filter;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.mockito.Mockito.*;

class LogoutFilterTest {

    private static LogoutFilter filter;

    @BeforeAll
    static void objMapping() {
        filter = new LogoutFilter();
    }

    @Test
    void invalidate_existed_session() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getSession(false)).thenReturn(session);

        filter.doFilter(request, response, filterChain);

        verify(session, times(1)).invalidate();
        verify(response, times(1)).sendRedirect(anyString());
    }

    @Test
    void invalidate_null_session() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getSession(false)).thenReturn(null);

        filter.doFilter(request, response, filterChain);

        verify(response, times(1)).sendRedirect(anyString());
    }

}
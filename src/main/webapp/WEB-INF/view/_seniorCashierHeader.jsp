<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<c:if test="${!empty(param.language)}">
    <c:set var="language" value="${param.language}" scope="session"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="locales"/>
<!doctype html>
<html lang="uk">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Header</title>
</head>
<body>
<%@ taglib prefix="r" uri="https://gitlab.com/volod_bol/the-cash-register" %>
<div class="p-3">
    <header class="d-flex flex-wrap justify-content-center py-2 mb-2 border-bottom">
        <p class="fw-bold fs-3 d-flex align-items-center mb-2 mb-md-0 me-md-auto text-decoration-none">
            <fmt:message key="header.greeting"/>, ${sessionScope.login}! (<r:writeRole role="${sessionScope.role}"
                                                                                       language="${language}"/>)
        </p>
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a href="#" class="nav-link" aria-current="page"
                   onclick="langEn()">
                    English
                </a></li>
            <li class="nav-item">
                <a href="#" class="nav-link" aria-current="page"
                   onclick="langUa()">
                    Українська
                </a></li>
            <div class="vr"></div>
            <li class="nav-item"><a
                    href="${pageContext.request.contextPath}/seniorcashier"
                    class="nav-link"
                    aria-current="page"><fmt:message key="header.navbar.main"/></a></li>
            <li class="nav-item"><a
                    href="${pageContext.request.contextPath}/seniorcashier/register-user"
                    class="nav-link"
                    aria-current="page"><fmt:message key="header.navbar.register"/></a></li>
            <li class="nav-item"><a
                    href="${pageContext.request.contextPath}/seniorcashier/download-report?reportType=X"
                    class="nav-link"
                    aria-current="page"><fmt:message key="header.navbar.report.x"/></a></li>
            <li class="nav-item"><a
                    href="${pageContext.request.contextPath}/seniorcashier/download-report?reportType=Z"
                    class="nav-link"
                    aria-current="page"><fmt:message key="header.navbar.report.z"/></a></li>
            <li class="nav-item"><a href="${pageContext.request.contextPath}/logout" class="nav-link active"
                                    aria-current="page"><fmt:message key="header.navbar.logout"/></a></li>
        </ul>
    </header>
</div>
<script>
    function langEn() {
        let url = document.URL;
        if (!url.includes('language=en')) {
            if (url.includes('language=ua')) {
                location.replace(url.replace('language=ua', 'language=en'))
            } else if (url.includes("?")) {
                location.replace(url + '&language=en')
            } else {
                location.replace(url + '?language=en')
            }
        }
    }

    function langUa() {
        let url = document.URL;
        if (!url.includes('language=ua')) {
            if (url.includes('language=en')) {
                location.replace(url.replace('language=en', 'language=ua'))
            } else if (url.includes('?')) {
                location.replace(url + '&language=ua')
            } else {
                location.replace(url + '?language=ua')
            }
        }
    }
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
</body>
</html>

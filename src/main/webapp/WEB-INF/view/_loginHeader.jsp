<%@ page contentType="text/html;charset=UTF-8" %>
<!doctype html>
<html lang="uk">
<head>
    <title>Title</title>
</head>
<body>
<div class="p-3">
    <header class="d-flex flex-wrap justify-content-center py-2 mb-2 border-bottom">
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a class="nav-link" href="${pageContext.request.contextPath}/login">
                    <img src="${pageContext.request.contextPath}/img/cash-register.png" width="24" height="24" class="d-inline-block"
                         alt="main page">
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link" aria-current="page"
                   onclick="langEn()">
                    English
                </a></li>
            <li class="nav-item">
                <a href="#" class="nav-link" aria-current="page"
                   onclick="langUa()">
                    Українська
                </a></li>
        </ul>
    </header>
</div>
<script>
    function langEn() {
        let url = document.URL;
        if (!url.includes('language=en')) {
            if (url.includes('language=ua')) {
                location.replace(url.replace('language=ua', 'language=en'))
            } else if (url.includes("?")) {
                location.replace(url + '&language=en')
            } else {
                location.replace(url + '?language=en')
            }
        }
    }

    function langUa() {
        let url = document.URL;
        if (!url.includes('language=ua')) {
            if (url.includes('language=en')) {
                location.replace(url.replace('language=en', 'language=ua'))
            } else if (url.includes('?')) {
                location.replace(url + '&language=ua')
            } else {
                location.replace(url + '?language=ua')
            }
        }
    }
</script>
</body>
</html>

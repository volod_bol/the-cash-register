<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${!empty(param.language)}">
    <c:set var="language" value="${param.language}" scope="session"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="locales"/>
<!doctype html>
<html lang="uk">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title><fmt:message key="storekeeper.title"/></title>
</head>
<body>
<jsp:include page="/WEB-INF/view/_header.jsp"/>
<div class="p-4">
    <h1><fmt:message key="storekeeper.title"/></h1>
    <c:if test="${!empty(goodsList)}" scope="request" var="list">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col"><fmt:message key="storekeeper.table.title"/></th>
            <th scope="col"><fmt:message key="storekeeper.table.quantity"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${requestScope.goodsList}" var="goods">
            <tr>
                <th scope="row"><c:out value="${goods.id}"/></th>
                <td><c:out value="${goods.title}"/></td>
                <td><c:out value="${goods.quantity}"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <nav aria-label="Navigation throw list of goods">
        <ul class="pagination">
            <c:choose>
                <c:when test="${param.offset == null || param.offset == 0}">
                    <li class="page-item disabled">
                        <a class="page-link"><fmt:message key="navigation.previous"/></a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li class="page-item"><a class="page-link"
                                             href="${pageContext.request.contextPath}/storekeeper?offset=${param.offset - requestScope.itemPerPageReq}"><fmt:message
                            key="navigation.previous"/></a>
                    </li>
                </c:otherwise>
            </c:choose>
            <c:forEach var="counter" begin="0" end="${requestScope.pages}">
                <c:choose>
                    <c:when test="${counter == 0 && (param.offset == null || param.offset == 0)}">
                        <li class="page-item active">
                            <a class="page-link">1</a>
                        </li>
                    </c:when>
                    <c:when test="${param.offset != null && (counter * requestScope.itemPerPageReq) == param.offset}">
                        <li class="page-item active">
                            <a class="page-link">${counter + 1}</a>
                        </li>
                    </c:when>
                    <c:when test="${(counter - 1) * requestScope.itemPerPageReq == requestScope.quantityOfGoods}">
                        <li class="page-item"><a class="page-link"
                                                 href="${pageContext.request.contextPath}/storekeeper?offset=${counter * requestScope.itemPerPageReq}">${counter + 1}</a>
                        </li>
                    </c:when>
                    <c:when test="${counter * requestScope.itemPerPageReq < requestScope.quantityOfGoods}">
                        <li class="page-item"><a class="page-link"
                                                 href="${pageContext.request.contextPath}/storekeeper?offset=${counter * requestScope.itemPerPageReq}">${counter + 1}</a>
                        </li>
                    </c:when>
                </c:choose>
            </c:forEach>
            <c:choose>
                <c:when test="${(param.offset == null || param.offset == 0) && (requestScope.itemPerPageReq == requestScope.quantityOfGoods)}">
                    <li class="page-item"><a class="page-link"
                                             href="${pageContext.request.contextPath}/storekeeper?offset=${requestScope.itemPerPageReq}"><fmt:message
                            key="navigation.next"/></a>
                    </li>
                </c:when>
                <c:when test="${param.offset + requestScope.itemPerPageReq > requestScope.quantityOfGoods}">
                    <li class="page-item disabled">
                        <a class="page-link"><fmt:message key="navigation.next"/></a>
                    </li>
                </c:when>
                <c:when test="${param.offset + requestScope.itemPerPageReq == requestScope.quantityOfGoods}">
                    <li class="page-item disabled">
                        <a class="page-link"><fmt:message key="navigation.next"/></a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li class="page-item"><a class="page-link"
                                             href="${pageContext.request.contextPath}/storekeeper?offset=${param.offset + requestScope.itemPerPageReq}"><fmt:message
                            key="navigation.next"/></a>
                    </li>
                </c:otherwise>
            </c:choose>
        </ul>
    </nav>
    <div class="w-25">
        <c:if test="${wrongGoodsInput}" scope="request" var="inputState">
            <p class="fs-4 text-danger"><fmt:message key="storekeeper.wrong.input"/></p>
        </c:if>

        <c:if test="${requestScope.lastGoodsStorekeeper != null}">
            <p class="fs-4"><fmt:message key="storekeeper.last.added.goods"/></p>
            <p class="fs-5">ID:
                <em>${requestScope.lastGoodsStorekeeper.id}</em></p>
            <p class="fs-5"><fmt:message key="storekeeper.last.added.title"/>
                <em>${requestScope.lastGoodsStorekeeper.title}</em></p>
            <p class="fs-5"><fmt:message key="storekeeper.last.added.quantity"/>
                <em><c:out value="${requestScope.lastGoodsStorekeeper.quantity}"/></em></p>
        </c:if>
        <c:if test="${requestScope.changedQuantity}">
            <p class="fs-4"><fmt:message key="storekeeper.goods.changed.quantity"/></p>
            <p class="fs-5">ID:
                <em>${requestScope.changedGoodsId}</em></p>
            <p class="fs-5"><fmt:message key="storekeeper.goods.changed.quantity.title"/>
                <em>${requestScope.changedGoodsTitle}</em></p>
            <p class="fs-5"><fmt:message key="storekeeper.goods.changed.quantity.new"/>
                <em>${requestScope.newQuantity}</em></p>
        </c:if>
        <form method="post" action="storekeeper?action=addGoods">
            <h4><fmt:message key="storekeeper.form.add.new.goods"/></h4>
            <div class="form-floating mb-3">
                <input type="text" class="form-control" name="title" id="title" placeholder="Title">
                <label for="title"><fmt:message key="storekeeper.form.add.new.goods.title"/></label>
            </div>
            <div class="form-floating">
                <input type="text" class="form-control" name="quantity" id="quantity" placeholder="Quantity">
                <label for="quantity"><fmt:message key="storekeeper.form.add.new.goods.quantity"/></label>
            </div>
            <br>
            <input class="btn btn-info btn-lg" type="submit"
                   value="<fmt:message key="storekeeper.form.add.new.goods.button"/>">
        </form>

        <br>
        <form method="post" action="storekeeper?action=changeGoods">
            <h4><fmt:message key="storekeeper.form.change.quantity.title"/></h4>
            <div class="form-floating mb-3">
                <input type="text" class="form-control" name="goodsId" id="goodsId" placeholder="Title">
                <label for="title"><fmt:message key="storekeeper.form.change.quantity.input.id.label"/></label>
            </div>
            <div class="form-floating">
                <input type="text" class="form-control" name="quantityChange" id="quantityChange"
                       placeholder="Quantity">
                <label for="quantityChange"><fmt:message
                        key="storekeeper.form.change.quantity.input.amount.label"/></label>
            </div>
            <br>
            <input class="btn btn-info btn-lg" type="submit"
                   value="<fmt:message key="storekeeper.form.change.quantity.button"/>">
        </form>

        <br>
        <form method="get" action="storekeeper">
            <h4><fmt:message key="storekeeper.pagination.title"/></h4>
            <select name="itemPerPageInput" class="form-select" aria-label="Items per page">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>
            </select>
            <br>
            <button type="submit" class="btn btn-success btn-lg"><fmt:message
                    key="storekeeper.pagination.button"/></button>
        </form>
    </div>
</div>
</c:if>
<c:if test="${empty(goodsList)}" scope="request" var="goodsList">
    <p class="text-center">
    <h2><fmt:message key="storekeeper.goodslist.empty"/></h2>
</c:if>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
</body>
</html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${!empty(param.language)}">
    <c:set var="language" value="${param.language}" scope="session"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="locales"/>
<!doctype html>
<html lang="uk">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title><fmt:message key="senior.cashier.title"/></title>
</head>
<body>
<jsp:include page="/WEB-INF/view/_seniorCashierHeader.jsp"/>
<div class="p-4">
    <figure class="text-center">
        <blockquote class="blockquote">
            <h1 class="display-2"><fmt:message key="senior.cashier.title"/></h1>
        </blockquote>
    </figure>
    <c:if test="${!empty(orderList)}" scope="request" var="list">
        <table class="table align-middle table-striped">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col"><fmt:message key="senior.cashier.table.cashier"/></th>
                <th scope="col"><fmt:message key="senior.cashier.table.goods"/></th>
                <th scope="col"><fmt:message key="senior.cashier.table.date"/></th>
                <th scope="col"><fmt:message key="senior.cashier.table.time"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${requestScope.orderList}" var="order">
                <tr>
                    <th scope="row"><c:out value="${order.id}"/></th>
                    <td><c:out value="${order.userLogin}"/></td>
                    <td>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col"><fmt:message key="cashier.form.select.name"/></th>
                                <th scope="col"><fmt:message key="cashier.form.label.amount"/></th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${order.goodsList}" var="goods">
                                <tr>
                                    <th scope="row"><c:out value="${goods.id}"/></th>
                                    <td><c:out value="${goods.title}"/></td>
                                    <td><c:out value="${goods.amount}"/></td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </td>
                    <td><c:out value="${order.date}"/></td>
                    <td><c:out value="${order.time}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <nav aria-label="Navigation throw list of orders">
            <ul class="pagination">
                <c:choose>
                    <c:when test="${param.offset == null || param.offset == 0}">
                        <li class="page-item disabled">
                            <a class="page-link"><fmt:message key="navigation.previous"/></a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="page-item"><a class="page-link"
                                                 href="${pageContext.request.contextPath}/seniorcashier?offset=${param.offset - requestScope.orderPerPageReq}"><fmt:message
                                key="navigation.previous"/></a>
                        </li>
                    </c:otherwise>
                </c:choose>
                <c:forEach var="counter" begin="0" end="${requestScope.pages}">
                    <c:choose>
                        <c:when test="${counter == 0 && (param.offset == null || param.offset == 0)}">
                            <li class="page-item active">
                                <a class="page-link">1</a>
                            </li>
                        </c:when>
                        <c:when test="${param.offset != null && (counter * requestScope.orderPerPageReq) == param.offset}">
                            <li class="page-item active">
                                <a class="page-link">${counter + 1}</a>
                            </li>
                        </c:when>
                        <c:when test="${(counter - 1) * requestScope.orderPerPageReq == requestScope.quantityOfOrders}">
                            <li class="page-item"><a class="page-link"
                                                     href="${pageContext.request.contextPath}/seniorcashier?offset=${counter * requestScope.orderPerPageReq}">${counter + 1}</a>
                            </li>
                        </c:when>
                        <c:when test="${counter * requestScope.orderPerPageReq < requestScope.quantityOfOrders}">
                            <li class="page-item"><a class="page-link"
                                                     href="${pageContext.request.contextPath}/seniorcashier?offset=${counter * requestScope.orderPerPageReq}">${counter + 1}</a>
                            </li>
                        </c:when>
                    </c:choose>
                </c:forEach>
                <c:choose>
                    <c:when test="${(param.offset == null || param.offset == 0) && (requestScope.orderPerPageReq == requestScope.quantityOfOrders)}">
                        <li class="page-item disabled">
                            <a class="page-link"><fmt:message key="navigation.next"/></a>
                        </li>
                    </c:when>
                    <c:when test="${param.offset + requestScope.orderPerPageReq > requestScope.quantityOfOrders}">
                        <li class="page-item disabled">
                            <a class="page-link"><fmt:message key="navigation.next"/></a>
                        </li>
                    </c:when>
                    <c:when test="${param.offset + requestScope.orderPerPageReq == requestScope.quantityOfOrders}">
                        <li class="page-item disabled">
                            <a class="page-link"><fmt:message key="navigation.next"/></a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="page-item"><a class="page-link"
                                                 href="${pageContext.request.contextPath}/seniorcashier?offset=${param.offset + requestScope.orderPerPageReq}"><fmt:message
                                key="navigation.next"/></a>
                        </li>
                    </c:otherwise>
                </c:choose>
            </ul>
        </nav>
        <div class="w-25">
            <c:if test="${incorrectInput}" scope="request" var="inputState">
                <p class="fs-4 text-danger"><fmt:message key="senior.cashier.wrong.input"/></p>
            </c:if>
            <c:if test="${requestScope.deletedOrder != null}">
                <p class="fs-4 text-danger"><fmt:message key="senior.cashier.canceled.check.title"/></p>
                <p class="fs-5 text-danger">
                    ID: <em>${deletedOrder.id}</em><br>
                    <fmt:message key="senior.cashier.canceled.check.cashier"/> <em>${deletedOrder.userLogin}</em><br>
                    <fmt:message key="senior.cashier.canceled.check.date"/> <em>${deletedOrder.date}</em><br>
                    <fmt:message key="senior.cashier.canceled.check.time"/> <em>${deletedOrder.time}</em>
                </p>
            </c:if>
            <c:if test="${deletedGoods != null}">
                <p class="fs-4 text-danger"><fmt:message key="senior.cashier.canceled.goods.title"/></p>
                <p class="fs-5 text-danger">
                    <fmt:message key="senior.cashier.canceled.goods.check.id"/> <em>${deletedFrom}</em><br>
                    <fmt:message key="senior.cashier.canceled.goods.id"/> <em>${deletedGoods.id}</em><br>
                    <fmt:message key="senior.cashier.canceled.goods.name"/> <em>${deletedGoods.title}</em><br>
                    <fmt:message key="senior.cashier.canceled.goods.amount"/> <em>${deletedGoods.amount}</em>
                </p>
            </c:if>
            <form method="post" action="seniorcashier?action=deleteOrder">
                <h4><fmt:message key="senior.cashier.form.cancel.check.title"/></h4>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" name="order_id" id="delete_order_id" placeholder="Order ID">
                    <label for="delete_order_id">ID</label>
                </div>
                <input class="btn btn-danger btn-lg" type="submit"
                       value="<fmt:message key="senior.cashier.form.cancel.check.button"/>">
            </form>
            <br/>
            <form method="post" action="seniorcashier?action=deleteGoods">
                <h4><fmt:message key="senior.cashier.form.cancel.goods.title"/></h4>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" name="delete_goods_order_id" id="delete_goods_id"
                           placeholder="Order ID">
                    <label for="delete_goods_id"><fmt:message
                            key="senior.cashier.form.cancel.goods.order.id.label"/></label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" name="goods_id" id="goods_id" placeholder="Goods ID">
                    <label for="goods_id"><fmt:message key="senior.cashier.form.cancel.goods.goods.id.label"/></label>
                </div>
                <input class="btn btn-danger btn-lg" type="submit"
                       value="<fmt:message key="senior.cashier.form.cancel.goods.button"/>">
            </form>
            <br/>
            <form method="get" action="seniorcashier">
                <h4><fmt:message key="senior.cashier.pagination.orders.page"/></h4>
                <select name="ordersPerPageInput" class="form-select" aria-label="Orders per page">
                    <option value="5">5</option>
                    <option value="10">10</option>
                    <option value="15">15</option>
                    <option value="20">20</option>
                </select>
                <br>
                <button type="submit" class="btn btn-success btn-lg"><fmt:message
                        key="senior.cashier.pagination.button"/></button>
            </form>
        </div>
    </c:if>
    <c:if test="${empty(requestScope.orderList)}">
        <p class="text-center fs-4"><fmt:message key="senior.cashier.empty.orders"/></p>
    </c:if>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
</body>
</html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:useBean id="goodsOrderList" scope="request" type="java.util.List"/>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${!empty(param.language)}">
    <c:set var="language" value="${param.language}" scope="session"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="locales"/>
<!doctype html>
<html lang="uk">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title><fmt:message key="cashier.review.title"/></title>
</head>
<body>
<jsp:include page="/WEB-INF/view/_header.jsp"/>
<div class="position-relative">
    <div class="position-absolute top-0 start-50 translate-middle-x">
        <h1><fmt:message key="cashier.review.order.title"/></h1>
        <c:if test="${!empty(goodsOrderList)}" scope="request" var="orderList">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col"><fmt:message key="storekeeper.table.title"/></th>
                    <th scope="col"><fmt:message key="cashier.form.label.amount"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${requestScope.goodsOrderList}" var="goodsOrder">
                    <tr>
                        <th scope="row"><c:out value="${goodsOrder.id}"/></th>
                        <td><c:out value="${goodsOrder.title}"/></td>
                        <td><c:out value="${goodsOrder.amount}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <form method="post" action="review-order?action=editPosition">
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" name="goodsIdentifierInput" id="IdentifierInput"
                           placeholder="Identifier">
                    <label for="IdentifierInput"><fmt:message key="cashier.review.identifier.label"/></label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" name="goodsAmount" id="AmountInput" placeholder="Amount">
                    <label for="AmountInput"><fmt:message key="cashier.review.amount.label"/></label>
                </div>
                <button type="submit" class="btn btn-secondary btn-lg"><fmt:message
                        key="cashier.review.change.amount.button"/></button>
            </form>
            <br/>
            <c:if test="${incorrectInputEditOrder}" scope="request" var="inputState">
                <p class="fs-5 text-danger"><fmt:message key="cashier.review.wrong.input"/></p>
            </c:if>
            <c:if test="${errorConfirmOrder}" scope="request" var="inputState">
                <p class="fs-5 text-danger"><fmt:message key="cashier.review.error.confirm"/></p>
            </c:if>
            <br/>
            <form method="post" action="review-order?action=confirmOrder">
                <button type="submit" class="btn btn-success btn-lg"><fmt:message
                        key="cashier.review.confirm.button"/></button>
            </form>
        </c:if>
        <c:if test="${empty(goodsOrderList)}" scope="request" var="orderList">
            <h3><fmt:message key="cashier.review.empty.order"/></h3>
            <a href="${pageContext.request.contextPath}/cashier/create-order"
               class="btn btn-primary btn-lg"><fmt:message key="cashier.review.empty.order.button"/></a>
        </c:if>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
</body>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:useBean id="goodsOrderList" scope="request" type="java.util.List"/>
<jsp:useBean id="lastGoods" scope="request" type="com.epam.elearn.cashregister.entity.GoodsOrder"/>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${!empty(param.language)}">
    <c:set var="language" value="${param.language}" scope="session"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="locales"/>
<!doctype html>
<html lang="uk">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title><fmt:message key="cashier.create.order.title"/></title>
</head>
<body>
<jsp:include page="/WEB-INF/view/_header.jsp"/>
<div class="position-relative">
    <div class="position-absolute top-0 start-50 translate-middle-x">
        <h1 class="display-2"><fmt:message key="cashier.create.order.title"/></h1>
        <form method="post" action="create-order">

            <p class="fs-3"><fmt:message key="cashier.form.select.goodsid"/></p>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="goodsIdentifier" value="id" id="radioId" checked>
                <label class="form-check-label" for="radioId">
                    <p class="fs-5">ID</p>
                </label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="goodsIdentifier" value="title" id="radioTitle">
                <label class="form-check-label" for="radioTitle">
                    <p class="fs-5"><fmt:message key="cashier.form.select.name"/></p>
                </label>
            </div>

            <div class="form-floating mb-3">
                <input type="text" class="form-control" name="goodsIdentifierInput" id="IdentifierInput"
                       placeholder="Identifier">
                <label for="IdentifierInput"><fmt:message key="cashier.form.label.identifier"/></label>
            </div>

            <div class="form-floating mb-3">
                <input type="text" class="form-control" name="goodsAmount" id="AmountInput" placeholder="Amount">
                <label for="AmountInput"><fmt:message key="cashier.form.label.amount"/></label>
            </div>

            <button type="submit" class="btn btn-secondary btn-lg"><fmt:message key="cashier.form.button"/></button>
        </form>
        <br>

        <c:if test="${!empty(goodsOrderList)}" scope="request" var="list">
            <p class="fs-4"><fmt:message key="cashier.goods.order"/> ${goodsOrderList.size()}</p>
            <p class="fs-5"><fmt:message key="cashier.goods.last.item"/></p>
            <p class="fs-5"><fmt:message key="cashier.goods.last.name"/> <em>${lastGoods.title}</em></p>
            <p class="fs-5"><fmt:message key="cashier.goods.last.amount"/> <em>${lastGoods.amount}</em></p>
        </c:if>

        <c:if test="${incorrectInputCreateOrder}" scope="request" var="inputState">
            <p class="fs-4 text-danger"><fmt:message key="cashier.wrong.input.first"/><br>
                <fmt:message key="cashier.wrong.input.second"/></p>
        </c:if>

        <form method="get" action="review-order">
            <button type="submit" class="btn btn-success btn-lg"><fmt:message key="cashier.review.button"/></button>
        </form>

        <br>
        <a href="${pageContext.request.contextPath}/cashier/goods" class="btn btn-info btn-lg"
           target="_blank"><fmt:message key="cashier.goods.button"/></a>

    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
</body>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${!empty(param.language)}">
    <c:set var="language" value="${param.language}" scope="session"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="locales"/>
<!doctype html>
<html lang="uk">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title><fmt:message key="senior.cashier.registration.title"/></title>
</head>
<body>
<jsp:include page="_loginHeader.jsp"/>
<div class="position-relative">
    <div class="position-absolute top-0 start-50 translate-middle-x w-25">
        <h1 class="display-3"><fmt:message key="senior.cashier.registration.title"/></h1>
        <form method="post" action="sign-up">
            <h4><fmt:message key="senior.cashier.registration.successful.role.label"/></h4>
            <select name="userRole" class="form-select form-select-lg mb-3" aria-label="User role">
                <option value="CASHIER"><fmt:message
                        key="senior.cashier.registration.successful.role.cashier"/></option>
                <option value="STOREKEEPER"><fmt:message
                        key="senior.cashier.registration.successful.role.storekeeper"/></option>
                <option value="SENIOR_CASHIER"><fmt:message
                        key="senior.cashier.registration.successful.role.senior.cashier"/></option>
            </select>
            <div class="form-floating">
                <input type="text" class="form-control" name="userLogin" id="userLogin" placeholder="Login"
                       value="${loginInput}">
                <label for="userLogin"><fmt:message key="senior.cashier.registration.form.login.label"/></label>
            </div>
            <c:if test="${incorrectLogin}" scope="request" var="inputState">
                <p class="fs-5 text-danger"><fmt:message key="senior.cashier.registration.form.wrong.login"/></p>
            </c:if>
            <c:if test="${isLoginUsed}" scope="request" var="inputState">
                <p class="fs-5 text-danger"><fmt:message key="senior.cashier.registration.form.error.used.login"/></p>
            </c:if>
            <p class="text-muted mb-3">
                <fmt:message key="senior.cashier.registration.form.rules.login.first"/> <kbd>_-</kbd> <fmt:message
                    key="senior.cashier.registration.form.rules.login.second"/>
            </p>
            <div class="form-floating">
                <input type="password" class="form-control" name="userPassword" id="userPassword"
                       placeholder="Password">
                <label for="userPassword"><fmt:message key="senior.cashier.registration.form.password.label"/></label>
            </div>
            <c:if test="${incorrectPassword}" scope="request" var="inputState">
                <p class="fs-5 text-danger"><fmt:message key="senior.cashier.registration.form.wrong.password"/></p>
            </c:if>
            <c:if test="${incorrectRepeatedPassword}" scope="request" var="inputState">
                <p class="fs-5 text-danger"><fmt:message key="senior.cashier.registration.form.error.repeated"/></p>
            </c:if>
            <p class="text-muted mb-3">
                <fmt:message key="senior.cashier.registration.form.rules.password"/>
            </p>
            <div class="form-floating mb-3">
                <input type="password" class="form-control" name="repeatedUserPassword" id="repeatedUserPassword"
                       placeholder="Repeat Password">
                <label for="repeatedUserPassword"><fmt:message
                        key="senior.cashier.registration.form.repeat.password.label"/></label>
            </div>
            <input class="btn btn-warning btn-lg" type="submit"
                   value="<fmt:message key="senior.cashier.registration.form.register.button"/>">
        </form>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
</body>
</html>
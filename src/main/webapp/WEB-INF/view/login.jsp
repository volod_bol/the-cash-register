<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ftm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<c:if test="${!empty(param.language)}">
    <c:set var="language" value="${param.language}" scope="session"/>
</c:if>
<c:if test="${language == null}">
    <c:set var="language" value="ua" scope="session"/>
</c:if>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="locales"/>
<!doctype html>
<html lang="uk">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title><fmt:message key="login.title"/></title>
</head>
<body>
<div class="p-3">
    <header class="d-flex flex-wrap justify-content-center py-2 mb-2 border-bottom">
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a href="#" class="nav-link" aria-current="page"
                   onclick="langEn()">
                    English
                </a></li>
            <li class="nav-item">
                <a href="#" class="nav-link" aria-current="page"
                   onclick="langUa()">
                    Українська
                </a></li>
        </ul>
    </header>
</div>
<c:if test="${isRegistered}">
    <h1 class="display-4 text-center text-success">
        <ftm:message key="login.registration.successful"/></h1>
</c:if>
<div class="position-absolute top-50 start-50 translate-middle">
    <h1 class="text-center"><fmt:message key="login.title"/></h1>
    <form method="post" action="login">
        <div class="form-floating mb-3">
            <input type="text" class="form-control" name="login" id="login" placeholder="Login">
            <label for="login"><fmt:message key="login.label.login"/></label>
        </div>
        <div class="form-floating">
            <input type="password" class="form-control" name="password" id="password" placeholder="Password">
            <label for="password"><fmt:message key="login.label.password"/></label>
        </div>
        <c:if test="${wrongLoginInput}" scope="request" var="inputState">
            <p class="fs-4 text-danger"><fmt:message key="login.error.first"/><br>
                <fmt:message key="login.error.second"/></p>
        </c:if>
        <br>
        <div class="d-flex justify-content-center">
            <input class="btn btn-primary btn-lg mb-3 w-75" type="submit"
                   value="<fmt:message key="login.button.login"/>">
        </div>
    </form>
    <div class="d-flex justify-content-center">
        <a class="btn btn-secondary btn-lg w-75" href="${pageContext.request.contextPath}/sign-up" role="link"><fmt:message
                key="login.button.registration"/></a>
    </div>
</div>
<script>
    function langEn() {
        let url = document.URL;
        if (!url.includes('language=en')) {
            if (url.includes('language=ua')) {
                location.replace(url.replace('language=ua', 'language=en'))
            } else if (url.includes("?")) {
                location.replace(url + '&language=en')
            } else {
                location.replace(url + '?language=en')
            }
        }
    }

    function langUa() {
        let url = document.URL;
        if (!url.includes('language=ua')) {
            if (url.includes('language=en')) {
                location.replace(url.replace('language=en', 'language=ua'))
            } else if (url.includes('?')) {
                location.replace(url + '&language=ua')
            } else {
                location.replace(url + '?language=ua')
            }
        }
    }
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
</body>
</html>
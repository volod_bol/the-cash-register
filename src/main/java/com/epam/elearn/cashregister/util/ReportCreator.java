package com.epam.elearn.cashregister.util;

import com.epam.elearn.cashregister.database.DBManager;
import com.epam.elearn.cashregister.entity.GoodsOrder;
import com.epam.elearn.cashregister.entity.Order;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class ReportCreator {

    private static final Logger LOGGER = LogManager.getLogger(ReportCreator.class);

    private static final String DATE_TIME_TABULATION = "%-45s %s";
    private static final String NUMBER_TABULATION = "%-52s %s";

    /**
     * Creates X and Z reports
     *
     * @param connection connection to database
     * @param userLogin  senior cashier username
     * @param reportType type of report
     * @return byte array output stream with pdf report
     */
    public ByteArrayOutputStream createReport(Connection connection, String userLogin, String reportType) {
        Document document = new Document(PageSize.A8, 3, 3, 5, 5);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            PdfWriter writer = PdfWriter.getInstance(document, outputStream);
            document.open();

            BaseFont baseFont = BaseFont.createFont("NotoSansMonoRegular.ttf", "cp1251", BaseFont.EMBEDDED);
            Font font = new Font(baseFont, 4);

            int purchasedGoods = DBManager.getCountPurchasedGoods(connection);
            int countOfOrders = DBManager.getCountOfOrders(connection);
            List<Order> allOrders = DBManager.getAllOrders(connection);

            int reportId;
            if (reportType.equals("Z")) {
                reportId = DBManager.confirmZReport(connection, userLogin);
            } else {
                reportId = DBManager.confirmXReport(connection, userLogin);
            }

            Paragraph border = new Paragraph("----------------------------------------------------------", font);
            Paragraph delimiter = new Paragraph("==========================================================", font);

            Paragraph companyTitle = new Paragraph("ТОВ \"МАНІЯ-ДРАЙВ\"", font);
            companyTitle.setAlignment(Element.ALIGN_CENTER);
            Paragraph location = new Paragraph("Київська обл.,Бориспільский р-н,с.Щасливе", font);
            location.setAlignment(Element.ALIGN_CENTER);
            Paragraph council = new Paragraph("с/р Щаслива", font);
            council.setAlignment(Element.ALIGN_CENTER);
            Paragraph street = new Paragraph("вул.Григорія Сковороди,21,заїзд зліва", font);
            street.setAlignment(Element.ALIGN_CENTER);
            Paragraph taxNum = new Paragraph("ПН 312334014299", font);
            taxNum.setAlignment(Element.ALIGN_CENTER);

            document.add(companyTitle);
            document.add(location);
            document.add(council);
            document.add(street);
            document.add(taxNum);

            document.add(border);
            Paragraph reportTitle;
            if (reportType.equals("Z")) {
                reportTitle = new Paragraph("Z звіт № " + reportId, font);
            } else {
                reportTitle = new Paragraph("X звіт № " + reportId, font);
            }
            reportTitle.setAlignment(Element.ALIGN_CENTER);
            document.add(reportTitle);
            document.add(border);

            LocalDate date = LocalDate.now();
            Paragraph dateGetting = new Paragraph(String.format(DATE_TIME_TABULATION, "Дата програмування:", date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))), font);
            document.add(dateGetting);
            document.add(delimiter);

            Paragraph ordersParagraph = new Paragraph("ЗАМОВЛЕННЯ", font);
            ordersParagraph.setAlignment(Element.ALIGN_CENTER);
            document.add(ordersParagraph);

            for (Order order : allOrders) {
                document.add(delimiter);
                Paragraph id = new Paragraph(String.format("%-28s %s", "ID замовлення:", order.getId()), font);
                document.add(id);

                Paragraph user = new Paragraph("Касир: " + order.getUserLogin(), font);
                document.add(user);

                Paragraph orderDate = new Paragraph(String.format(DATE_TIME_TABULATION, "Дата видачі:", order.getDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))), font);
                document.add(orderDate);

                Paragraph orderTime = new Paragraph(String.format(DATE_TIME_TABULATION, "Час видачі:", order.getTime().format(DateTimeFormatter.ofPattern("HH:mm:ss"))), font);
                document.add(orderTime);

                Paragraph goodsParagraph = new Paragraph("ТОВАРИ ЗАМОВЛЕННЯ", font);
                goodsParagraph.setAlignment(Element.ALIGN_CENTER);
                document.add(goodsParagraph);

                List<GoodsOrder> goodsList = order.getGoodsList();
                for (GoodsOrder goods : goodsList) {
                    document.add(border);
                    Paragraph goodsId = new Paragraph(String.format(NUMBER_TABULATION, "ID:", goods.getId()), font);
                    document.add(goodsId);
                    Paragraph goodsTitle = new Paragraph(String.format("%s %s", "Назва:", goods.getTitle()), font);
                    document.add(goodsTitle);
                    Paragraph goodsAmount = new Paragraph(String.format(NUMBER_TABULATION, "Кількість:", goods.getAmount()), font);
                    document.add(goodsAmount);
                }
            }

            document.add(delimiter);
            Paragraph amountOfPurchasedGoods = new Paragraph(String.format(NUMBER_TABULATION, "Кількість проданих товарів:", purchasedGoods), font);
            document.add(amountOfPurchasedGoods);
            Paragraph amountOfOrders = new Paragraph(String.format(NUMBER_TABULATION, "Кількість чеків:", countOfOrders), font);
            document.add(amountOfOrders);
            document.add(border);

            document.close();
            writer.close();
        } catch (IOException | DocumentException e) {
            LOGGER.error(() -> "Error during generation report, report type: " + reportType, e);
        }

        return outputStream;
    }

}
package com.epam.elearn.cashregister.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Properties;

/**
 * Class read key from properties prop thanks to which we encrypt
 * and decrypt passwords
 */
public class PasswordEncryptor {

    private static final Logger LOGGER = LogManager.getLogger(PasswordEncryptor.class);

    private byte[] key;

    private PasswordEncryptor() {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try (InputStream inputStream = classLoader.getResourceAsStream("app.properties")) {
            Properties properties = getProperties(inputStream);
            this.key = KeyUtils.hexStringToByteArray(properties.getProperty("key"));
            LOGGER.info("KEY received!");
        } catch (IOException e) {
            LOGGER.error("Cannot get app.properties as input stream!", e);
        }
    }

    public PasswordEncryptor(String key) {
        this.key = KeyUtils.hexStringToByteArray(key);
    }

    private static class SingletonHelper {
        private static final PasswordEncryptor INSTANCE = new PasswordEncryptor();
    }

    public static PasswordEncryptor getInstance() {
        return SingletonHelper.INSTANCE;
    }

    private Properties getProperties(InputStream inputStream) {
        Properties properties = new Properties();
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            LOGGER.error("Error during read app.properties", e);
        }
        return properties;
    }

    public String encryptPassword(String password) {
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, KeyUtils.AES_GEN);
        try {
            Cipher cipher = Cipher.getInstance(KeyUtils.AES_GEN);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, cipher.getParameters());
            return KeyUtils.byteArrayToHexString(cipher.doFinal(password.getBytes(StandardCharsets.UTF_8)));
        } catch (GeneralSecurityException e) {
            LOGGER.error(() -> String.format("Cannot encrypt password: %s", password), e);
        }
        return null;
    }

    public String decryptPassword(String password) {
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, KeyUtils.AES_GEN);
        try {
            Cipher cipher = Cipher.getInstance(KeyUtils.AES_GEN);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            return new String(cipher.doFinal(KeyUtils.hexStringToByteArray(password)));
        } catch (GeneralSecurityException e) {
            LOGGER.error(() -> String.format("Cannot decrypt password: %s", password), e);
        }
        return null;
    }

    /**
     * Util class that convert String to hex and back and generate new keys for properties
     */
    public static final class KeyUtils {
        public static final String AES_GEN = "AES";

        private KeyUtils() {
            throw new IllegalStateException();
        }

        private static String byteArrayToHexString(byte[] bytes) {
            StringBuilder builder = new StringBuilder();
            for (byte b : bytes) {
                builder.append(String.format("%02X", b));
            }
            return builder.toString().toUpperCase();
        }

        private static byte[] hexStringToByteArray(String s) {
            byte[] bytes = new byte[s.length() / 2];
            for (int i = 0; i < bytes.length; i++) {
                int index = i * 2;
                int v = Integer.parseInt(s.substring(index, index + 2), 16);
                bytes[i] = (byte) v;
            }
            return bytes;
        }

        public static String generateKey() throws NoSuchAlgorithmException {
            KeyGenerator keyGenerator = KeyGenerator.getInstance(KeyUtils.AES_GEN);
            keyGenerator.init(256, SecureRandom.getInstanceStrong());
            SecretKey secretKey = keyGenerator.generateKey();
            return KeyUtils.byteArrayToHexString(secretKey.getEncoded());
        }

    }
}

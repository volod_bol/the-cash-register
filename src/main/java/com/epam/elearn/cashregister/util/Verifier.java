package com.epam.elearn.cashregister.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Verifier {

    private static final Logger LOGGER = LogManager.getLogger(Verifier.class);

    /**
     * Checking if goods input for order correct
     *
     * @param identifier      type of identifier (id or title)
     * @param identifierInput identifier
     * @param amount          amount of goods
     * @return true if input correct, false if incorrect
     */
    public boolean isOrderInputCorrect(String identifier, String identifierInput, String amount) {
        if (identifier.equals("id") && !isIdInputCorrect(identifierInput)) {
            return false;
        }
        return isNumberCorrect(amount);
    }

    /**
     * Checking if goods title and amount for order correct
     *
     * @param title    goods title
     * @param quantity goods quantity
     * @return true if input correct, false if incorrect
     */
    public boolean isGoodsOrderCorrect(String title, String quantity) {
        if (!isNumberCorrect(quantity)) {
            return false;
        }
        return !title.isBlank();
    }

    /**
     * Checking if id input correct
     *
     * @param inputId id
     * @return true if input correct, false if incorrect
     */
    public boolean isIdInputCorrect(String inputId) {
        try {
            if (Integer.parseInt(inputId) < 1)
                return false;
        } catch (NumberFormatException e) {
            LOGGER.error(() -> "Wrong input for id: " + inputId);
            return false;
        }
        return true;
    }

    /**
     * Checking if order and goods id input correct
     *
     * @param orderIdInput order id
     * @param goodsIdInput goods id
     * @return true if input correct, false if incorrect
     */
    public boolean isDeleteGoodsOrderInputCorrect(String orderIdInput, String goodsIdInput) {
        try {
            if (Integer.parseInt(orderIdInput) < 1 || Integer.parseInt(goodsIdInput) < 1) {
                return false;
            }
        } catch (NumberFormatException e) {
            LOGGER.error(() -> String.format("Wrong input, cannot delete goods in order, orderId: %s, goodsId: %s", orderIdInput, goodsIdInput));
            return false;
        }
        return true;
    }

    /**
     * Checking if login input correct
     *
     * @param login login
     * @return true if input correct, false if incorrect
     */
    public boolean isLoginCorrect(String login) {
        if (login.isBlank()) {
            return false;
        }
        return login.matches("[a-z0-9_-]{5,16}");
    }

    /**
     * Checking if password input correct
     *
     * @param password password
     * @return true if input correct, false if incorrect
     */
    public boolean isPasswordCorrect(String password) {
        if (password.isBlank()) {
            return false;
        }
        return password.matches("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,}");
    }

    /**
     * Checking if number (amount, quantity) input correct
     *
     * @param number number input
     * @return true if input correct, false if incorrect
     */
    public boolean isNumberCorrect(String number) {
        number = number.replace(",", ".");
        try {
            if (Double.parseDouble(number) < 0)
                return false;
        } catch (NumberFormatException e) {
            String finalNumber = number;
            LOGGER.error(() -> "Wrong input for number: " + finalNumber);
            return false;
        }
        return true;
    }

}

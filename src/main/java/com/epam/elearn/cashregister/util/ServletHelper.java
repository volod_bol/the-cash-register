package com.epam.elearn.cashregister.util;

import com.epam.elearn.cashregister.database.ConnectionPoolPostgres;
import com.epam.elearn.cashregister.database.DBManager;
import com.epam.elearn.cashregister.entity.GoodsOrder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ServletHelper {

    private static final String ITEM_PER_PAGE_REG = "itemPerPageReq";
    private static final String ORDER_PER_PAGE_REG = "orderPerPageReq";

    /**
     * Removes information about order after confirming order
     *
     * @param session session
     */
    public void removeOrderInfo(HttpSession session) {
        session.removeAttribute("incorrectInputCreateOrder");
        session.removeAttribute("incorrectInputEditOrder");
        session.removeAttribute("goodsOrderList");
        session.removeAttribute("errorConfirmOrder");
    }

    /**
     * Get order from session and set it into request
     *
     * @param request request
     * @param listAttribute order list
     */
    public void setOrdersFromSessionToRequest(HttpServletRequest request, String listAttribute) {
        List<GoodsOrder> goodsOrderList = (List<GoodsOrder>) request.getSession().getAttribute(listAttribute);
        if (goodsOrderList != null) {
            request.setAttribute(listAttribute, goodsOrderList);
        } else {
            goodsOrderList = new ArrayList<>();
            request.getSession().setAttribute(listAttribute, goodsOrderList);
            request.setAttribute(listAttribute, goodsOrderList);
        }
    }

    /**
     * Get request and key of session attribute and then get object
     * under this key and set it to request
     *
     * @param request request
     * @param sessionAttribute session attribute
     */
    public void markIfWrongInput(HttpServletRequest request, String sessionAttribute) {
        Object incorrectInput = request.getSession().getAttribute(sessionAttribute);
        if (incorrectInput != null) {
            request.setAttribute(sessionAttribute, incorrectInput);
            request.getSession().removeAttribute(sessionAttribute);
        }
    }

    /**
     * Get list of goods from database depends on pagination settings and show them on page
     *
     * @param request request
     */
    public void renderGoods(HttpServletRequest request) {
        int itemPerPage = 5;
        String itemPerPageInput = request.getParameter("itemPerPageInput");
        if (itemPerPageInput != null) {
            request.getSession().setAttribute("itemPerPageSession", itemPerPageInput);
            request.setAttribute(ITEM_PER_PAGE_REG, itemPerPageInput);
            itemPerPage = Integer.parseInt(itemPerPageInput);
        } else {
            Object itemPerPageSession = request.getSession().getAttribute("itemPerPageSession");
            if (itemPerPageSession != null) {
                request.setAttribute(ITEM_PER_PAGE_REG, itemPerPageSession);
                itemPerPage = Integer.parseInt((String) itemPerPageSession);
            } else {
                request.setAttribute(ITEM_PER_PAGE_REG, itemPerPage);
            }
        }

        Connection connection = ((ConnectionPoolPostgres) request.getServletContext().getAttribute("connectionPool")).getConnection();
        int quantityOfGoods = DBManager.getCountOfGoods(connection);
        int pages = (int) Math.round(quantityOfGoods / (double) itemPerPage);
        request.setAttribute("pages", pages);
        request.setAttribute("quantityOfGoods", quantityOfGoods);
        String offset = request.getParameter("offset");
        if (offset != null) {
            request.setAttribute("goodsList", DBManager.getAllGoods(connection, itemPerPage, Integer.parseInt(offset)));
        } else {
            request.setAttribute("goodsList", DBManager.getAllGoods(connection, itemPerPage, 0));
        }

        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get list of orders from database depends on pagination settings and show them on page
     *
     * @param request request
     */
    public void renderOrders(HttpServletRequest request) {
        int ordersPerPage = 5;
        String ordersPerPageInput = request.getParameter("ordersPerPageInput");
        if (ordersPerPageInput != null) {
            request.getSession().setAttribute("ordersPerPageSession", ordersPerPageInput);
            request.setAttribute(ORDER_PER_PAGE_REG, ordersPerPageInput);
            ordersPerPage = Integer.parseInt(ordersPerPageInput);
        } else {
            Object ordersPerPageSession = request.getSession().getAttribute("ordersPerPageSession");
            if (ordersPerPageSession != null) {
                request.setAttribute(ORDER_PER_PAGE_REG, ordersPerPageSession);
                ordersPerPage = Integer.parseInt((String) ordersPerPageSession);
            } else {
                request.setAttribute(ORDER_PER_PAGE_REG, ordersPerPage);
            }
        }

        Connection connection = ((ConnectionPoolPostgres) request.getServletContext().getAttribute("connectionPool")).getConnection();
        int quantityOfOrders = DBManager.getCountOfOrders(connection);
        int pages = (int) Math.round(quantityOfOrders / (double) ordersPerPage);
        request.setAttribute("pages", pages);
        request.setAttribute("quantityOfOrders", quantityOfOrders);
        String offset = request.getParameter("offset");
        if (offset != null) {
            request.setAttribute("orderList", DBManager.getAllOrders(connection, ordersPerPage, Integer.parseInt(offset)));
        } else {
            request.setAttribute("orderList", DBManager.getAllOrders(connection, ordersPerPage, 0));
        }

        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

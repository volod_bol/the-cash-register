package com.epam.elearn.cashregister.servlets.filter;

import com.epam.elearn.cashregister.database.ConnectionPoolPostgres;
import com.epam.elearn.cashregister.util.ReportCreator;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Gives download one of reports to user
 */
public class ReportDownloadFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        String login = (String) req.getSession().getAttribute("login");
        String reportType = req.getParameter("reportType");
        ReportCreator reportCreator = new ReportCreator();

        Connection connection = ((ConnectionPoolPostgres) req.getServletContext().getAttribute("connectionPool")).getConnection();
        ByteArrayOutputStream pdfOutputStream = reportCreator.createReport(connection, login, reportType);
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        res.setContentType("application/pdf");
        res.setContentLength(pdfOutputStream.size());

        LocalDate date = LocalDate.now();
        LocalTime time = LocalTime.now();
        String headerKey = "Content-Disposition";
        String headerValue;
        if (reportType.equals("Z")) {
            headerValue = String.format("attachment; filename=\"%s\"", login + "_" + date + "_" + time.format(DateTimeFormatter.ofPattern("HH-mm-ss")) + "_z_report.pdf");
        } else {
            headerValue = String.format("attachment; filename=\"%s\"", login + "_" + date + "_" + time.format(DateTimeFormatter.ofPattern("HH-mm-ss")) + "_x_report.pdf");
        }
        res.setHeader(headerKey, headerValue);

        try (ServletOutputStream resOutputStream = res.getOutputStream()) {
            pdfOutputStream.writeTo(resOutputStream);
            resOutputStream.flush();
            pdfOutputStream.close();
        }

    }
}

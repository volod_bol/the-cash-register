package com.epam.elearn.cashregister.servlets.servlet;

import com.epam.elearn.cashregister.database.ConnectionPoolPostgres;
import com.epam.elearn.cashregister.database.DBManager;
import com.epam.elearn.cashregister.entity.GoodsOrder;
import com.epam.elearn.cashregister.util.Verifier;
import com.epam.elearn.cashregister.util.ServletHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class CashierReviewOrderServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(CashierReviewOrderServlet.class);

    private static final String INCORRECT_INPUT = "incorrectInputEditOrder";
    private static final String ORDER_LIST = "goodsOrderList";

    private final ServletHelper servletHelper = new ServletHelper();
    private final Verifier verifier = new Verifier();

    /**
     * Serve get http method on cashier review page. Show page of reviewing new order.
     *
     * @param request  request
     * @param response response
     * @throws ServletException ServletException
     * @throws IOException      IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        servletHelper.markIfWrongInput(request, INCORRECT_INPUT);
        servletHelper.markIfWrongInput(request, "errorConfirmOrder");

        servletHelper.setOrdersFromSessionToRequest(request, ORDER_LIST);

        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/WEB-INF/view/roles/cashier/reviewOrder.jsp");
        requestDispatcher.forward(request, response);
    }

    /**
     * Serve post http method on cashier review page. Can edit and confirm order.
     *
     * @param request  request
     * @param response response
     * @throws IOException IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String action = request.getParameter("action");
        String url = "/cashier/review-order";
        List<GoodsOrder> goodsOrderList = (List<GoodsOrder>) request.getSession().getAttribute(ORDER_LIST);
        switch (action) {
            case "editPosition":
                editGoodsAmount(request, goodsOrderList);
                break;
            case "confirmOrder":
                url = confirmOrder(request, url, goodsOrderList);
                break;
            default:
                LOGGER.error("Cannot recognize reviewOrder action");
        }
        response.sendRedirect(request.getContextPath() + url);
    }

    /**
     * Confirm order and save information about order to database.
     *
     * @param request        request
     * @param url            redirect url
     * @param goodsOrderList order list with goods
     * @return redirect url to creating new order
     */
    private String confirmOrder(HttpServletRequest request, String url, List<GoodsOrder> goodsOrderList) {
        Connection connection = ((ConnectionPoolPostgres) getServletContext().getAttribute("connectionPool")).getConnection();
        if (DBManager.confirmOrder(connection, goodsOrderList, (String) request.getSession().getAttribute("login"))) {
            servletHelper.removeOrderInfo(request.getSession());
            url = "/cashier/create-order";
        } else {
            StringBuilder stringBuilder = new StringBuilder();
            goodsOrderList.forEach(goodsOrder -> stringBuilder.append(goodsOrder).append("\n"));
            request.getSession().setAttribute("errorConfirmOrder", true);
            LOGGER.info(() -> String.format("Error during confirming order. goodsOrderList: %s", stringBuilder));
        }

        try {
            connection.close();
        } catch (SQLException e) {
            LOGGER.error("Can not close connection", e);
        }

        return url;
    }

    /**
     * Change amount of position in order
     *
     * @param request        request
     * @param goodsOrderList order list with goods
     */
    private void editGoodsAmount(HttpServletRequest request, List<GoodsOrder> goodsOrderList) {
        String goodsIdentifierInput = request.getParameter("goodsIdentifierInput");
        String goodsAmount = request.getParameter("goodsAmount");
        if (verifier.isOrderInputCorrect("id", goodsIdentifierInput, goodsAmount)) {
            changeAmount(request.getSession(), goodsOrderList, goodsIdentifierInput, goodsAmount);
        } else {
            request.getSession().setAttribute(INCORRECT_INPUT, true);
            LOGGER.info(() -> String.format("Cannot edit order - wrong input, id: %s, amount: %s",
                    goodsIdentifierInput, goodsAmount));
        }
    }

    /**
     * Change amount of current goods
     *
     * @param session        user session
     * @param goodsOrderList order list with goods
     * @param id             id of goods
     * @param amount         new amount of goods
     */
    private void changeAmount(HttpSession session, List<GoodsOrder> goodsOrderList, String id, String amount) {
        int goodsId = Integer.parseInt(id);
        goodsOrderList.stream()
                .filter(goodsOrder -> goodsOrder.getId() == goodsId)
                .findFirst()
                .ifPresentOrElse(
                        goodsOrder -> {
                            goodsOrder.setAmount(amount);
                            LOGGER.info(() -> String.format("Changed item in order id: %s, title: %s, old amount %s, new amount %s",
                                    goodsOrder.getId(), goodsOrder.getTitle(), goodsOrder.getAmount(), amount));
                        },
                        () -> session.setAttribute(INCORRECT_INPUT, true));
    }

}

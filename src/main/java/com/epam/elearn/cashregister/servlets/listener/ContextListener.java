package com.epam.elearn.cashregister.servlets.listener;

import com.epam.elearn.cashregister.database.ConnectionPoolPostgres;
import com.epam.elearn.cashregister.util.PasswordEncryptor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

@WebListener
public class ContextListener implements ServletContextListener {

    private static final Logger LOGGER = LogManager.getLogger(ContextListener.class);
    private ConnectionPoolPostgres connectionPool;

    /**
     * When application starts, create connection pool and password encryptor.
     * Then developer has access to these objects.
     *
     * @param sce Servlet Context Event
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext servletContext = sce.getServletContext();

        connectionPool = ConnectionPoolPostgres.getInstance();
        servletContext.setAttribute("connectionPool", connectionPool);
        LOGGER.info("Connection pool initialized success");

        PasswordEncryptor passwordEncryptor = PasswordEncryptor.getInstance();
        servletContext.setAttribute("passwordUtils", passwordEncryptor);
        LOGGER.info("Password utils initialized success");

        LOGGER.info("Context initialized success");
    }

    /**
     * When application stops, connection pool to database will be shutdown and
     * all drivers will be deregistered.
     *
     * @param sce Servlet Context Event
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        connectionPool.shutdown();
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            try {
                DriverManager.deregisterDriver(driver);
                LOGGER.info(() -> "Driver deregistered: " + driver);
            } catch (SQLException e) {
                LOGGER.error(String.format("Error deregister driver: %s", driver), e);
            }
        }
    }
}

package com.epam.elearn.cashregister.servlets.servlet;

import com.epam.elearn.cashregister.database.ConnectionPoolPostgres;
import com.epam.elearn.cashregister.database.DBManager;
import com.epam.elearn.cashregister.entity.User;
import com.epam.elearn.cashregister.util.PasswordEncryptor;
import com.epam.elearn.cashregister.util.ServletHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class LoginServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(LoginServlet.class);

    private static final String WRONG_LOGIN_INPUT = "wrongLoginInput";
    private static final String LOGIN_URL = "/login";

    private final ServletHelper servletHelper = new ServletHelper();

    /**
     * Serve get http method of login page. Show login page for user.
     *
     * @param request  request
     * @param response response
     * @throws ServletException ServletException
     * @throws IOException      IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Object successRegistration = request.getSession().getAttribute("successRegistration");
        if (successRegistration != null) {
            request.getSession().removeAttribute("successRegistration");
            request.setAttribute("isRegistered", true);
        }

        servletHelper.markIfWrongInput(request, WRONG_LOGIN_INPUT);

        String role = (String) request.getSession().getAttribute("role");
        if (role != null) {
            redirectRole(request, response, role);
        } else {
            RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/WEB-INF/view/login.jsp");
            requestDispatcher.forward(request, response);
        }
    }

    /**
     * Serve post http method of login page. Authorizes users to system.
     *
     * @param request  request
     * @param response response
     * @throws IOException IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        if (!login.isBlank() && !password.isBlank()) {
            processLogin(request, response, login, password);
        } else {
            request.getSession().setAttribute(WRONG_LOGIN_INPUT, true);
            response.sendRedirect(request.getContextPath() + LOGIN_URL);
        }
    }

    /**
     * Trying to find user in database and then equal input and saved in database passwords.
     * If user don't exist or password wrong request save attribute about wrong input and show
     * it on page for user.
     *
     * @param request  request
     * @param response response
     * @param login    user login
     * @param password user password
     * @throws IOException IOException
     */
    private void processLogin(HttpServletRequest request, HttpServletResponse response, String login, String password) throws IOException {
        PasswordEncryptor passwordEncryptor = (PasswordEncryptor) request.getServletContext().getAttribute("passwordUtils");
        Connection connection = ((ConnectionPoolPostgres) request.getServletContext().getAttribute("connectionPool")).getConnection();

        User user = DBManager.getUser(connection, login);
        try {
            connection.close();
        } catch (SQLException e) {
            LOGGER.error("Cannot close connection to database", e);
        }

        String encryptedInputPassword = passwordEncryptor.encryptPassword(password);
        if (user != null && user.getPassword().equals(encryptedInputPassword)) {
            request.getSession().setAttribute("login", user.getLogin());
            request.getSession().setAttribute("role", user.getRole().toString());
            redirectRole(request, response, user.getRole().toString());
        } else {
            request.getSession().setAttribute(WRONG_LOGIN_INPUT, true);
            LOGGER.error(() -> String.format("Wrong input, cannot authorize user, login: %s, password: %s", login, encryptedInputPassword));
            response.sendRedirect(request.getContextPath() + LOGIN_URL);
        }
    }

    /**
     * Depends on user role redirect they on required page
     *
     * @param request  request
     * @param response response
     * @param role     usr role in system
     * @throws IOException IOException
     */
    private void redirectRole(HttpServletRequest request, HttpServletResponse response, String role) throws IOException {
        switch (role) {
            case "CASHIER":
                response.sendRedirect(request.getContextPath() + "/cashier/create-order");
                break;
            case "STOREKEEPER":
                response.sendRedirect(request.getContextPath() + "/storekeeper");
                break;
            case "SENIOR_CASHIER":
                response.sendRedirect(request.getContextPath() + "/seniorcashier");
                break;
            default:
                LOGGER.error("Cannot recognize role: redirect to login page");
                response.sendRedirect(request.getContextPath() + LOGIN_URL);
        }
    }

}

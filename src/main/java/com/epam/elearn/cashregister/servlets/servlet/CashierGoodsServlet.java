package com.epam.elearn.cashregister.servlets.servlet;

import com.epam.elearn.cashregister.util.ServletHelper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CashierGoodsServlet extends HttpServlet {

    private final ServletHelper servletHelper = new ServletHelper();

    /**
     * Serve get http method on cashier goods page. Show pages with goods.
     *
     * @param request  request
     * @param response response
     * @throws ServletException ServletException
     * @throws IOException      IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        servletHelper.renderGoods(request);

        ServletContext servletContext = getServletContext();
        RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher("/WEB-INF/view/roles/cashier/viewGoods.jsp");
        requestDispatcher.forward(request, response);
    }

}

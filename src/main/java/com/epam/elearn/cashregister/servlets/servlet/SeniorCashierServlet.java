package com.epam.elearn.cashregister.servlets.servlet;

import com.epam.elearn.cashregister.database.ConnectionPoolPostgres;
import com.epam.elearn.cashregister.database.DBManager;
import com.epam.elearn.cashregister.entity.GoodsOrder;
import com.epam.elearn.cashregister.entity.Order;
import com.epam.elearn.cashregister.util.Verifier;
import com.epam.elearn.cashregister.util.ServletHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class SeniorCashierServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(SeniorCashierServlet.class);

    private static final String DELETED_FROM = "deletedFrom";
    private static final String DELETED_ORDER = "deletedOrder";
    private static final String DELETED_GOODS = "deletedGoods";
    private static final String INCORRECT_INPUT = "incorrectInput";

    private final ServletHelper servletHelper = new ServletHelper();
    private final Verifier verifier = new Verifier();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        servletHelper.renderOrders(request);
        servletHelper.markIfWrongInput(request, INCORRECT_INPUT);
        HttpSession session = request.getSession();

        showDeletedItems(request, session);

        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/WEB-INF/view/roles/seniorcashier/seniorcashierMain.jsp");
        requestDispatcher.forward(request, response);
    }

    /**
     * Serve post http method on senior cashier page. Can delete order or delete some goods from order.
     *
     * @param request  request
     * @param response response
     * @throws IOException IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String action = request.getParameter("action");
        HttpSession session = request.getSession();
        switch (action) {
            case "deleteOrder":
                deleteOrder(request, session);
                break;
            case "deleteGoods":
                deleteGoodsFromOrder(request, session);
                break;
            default:
                LOGGER.error(() -> String.format("Cannot recognize action (senior cashier), action: %s", action));
        }
        response.sendRedirect(request.getContextPath() + "/seniorcashier");
    }

    /**
     * Deleting some order from database. If input incorrect, session saved attribute with
     * information about it and show on page
     *
     * @param request request
     * @param session session
     */
    private void deleteOrder(HttpServletRequest request, HttpSession session) {
        String deleteOrderId = request.getParameter("order_id");
        if (verifier.isIdInputCorrect(deleteOrderId)) {
            addDeletedOrderToSession(session, deleteOrderId);
        } else {
            session.setAttribute(INCORRECT_INPUT, true);
            LOGGER.error(() -> "Wrong input, cannot delete order, orderId: " + deleteOrderId);
        }
    }

    /**
     * Deleting order and save deleted order into database. If input incorrect, session saved attribute with
     * information about it and show on page
     *
     * @param session       session
     * @param deleteOrderId order id
     */
    private void addDeletedOrderToSession(HttpSession session, String deleteOrderId) {
        Connection connection = ((ConnectionPoolPostgres) getServletContext().getAttribute("connectionPool")).getConnection();
        Order order = DBManager.deleteOrder(connection, Integer.parseInt(deleteOrderId));
        if (order != null) {
            session.setAttribute(DELETED_ORDER, order);
        } else {
            session.setAttribute(INCORRECT_INPUT, true);
            LOGGER.error(() -> "Cannot delete order, bad input, orderId: " + deleteOrderId);
        }
        try {
            connection.close();
        } catch (SQLException e) {
            LOGGER.error("Can not close connection", e);
        }
    }

    /**
     * Deleting goods from order
     *
     * @param request request
     * @param session session
     */
    private void deleteGoodsFromOrder(HttpServletRequest request, HttpSession session) {
        String orderId = request.getParameter("delete_goods_order_id");
        String goodsId = request.getParameter("goods_id");
        if (verifier.isDeleteGoodsOrderInputCorrect(orderId, goodsId)) {
            addDeletedGoodsToSession(session, orderId, goodsId);
        } else {
            session.setAttribute(INCORRECT_INPUT, true);
            LOGGER.error(() -> String.format("Wrong input, cannot delete goods in order, orderId: %s, goodsId: %s", orderId, goodsId));
        }
    }

    /**
     * Deleting goods from order and save it into session that show information
     * about deleted goods on page to user
     *
     * @param session session
     * @param orderId order id
     * @param goodsId goods id
     */
    private void addDeletedGoodsToSession(HttpSession session, String orderId, String goodsId) {
        Connection connection = ((ConnectionPoolPostgres) getServletContext().getAttribute("connectionPool")).getConnection();
        GoodsOrder goodsOrder = DBManager.deleteGoodsInOrder(connection, Integer.parseInt(orderId), Integer.parseInt(goodsId));
        if (goodsOrder != null) {
            session.setAttribute(DELETED_GOODS, goodsOrder);
            session.setAttribute(DELETED_FROM, orderId);
        } else {
            session.setAttribute(INCORRECT_INPUT, true);
            LOGGER.error(() -> String.format("Wrong input, cannot delete goods in order, orderId: %s, goodsId: %s", orderId, goodsId));
        }
        try {
            connection.close();
        } catch (SQLException e) {
            LOGGER.error("Can not close connection", e);
        }
    }

    /**
     * Get information about deleted goods/order from session
     * and show it on page
     *
     * @param request request
     * @param session session
     */
    private void showDeletedItems(HttpServletRequest request, HttpSession session) {
        Object deletedOrder = session.getAttribute(DELETED_ORDER);
        if (deletedOrder != null) {
            request.setAttribute(DELETED_ORDER, deletedOrder);
            session.removeAttribute(DELETED_ORDER);
        }
        Object deletedGoods = session.getAttribute(DELETED_GOODS);
        if (deletedGoods != null) {
            request.setAttribute(DELETED_GOODS, deletedGoods);
            request.setAttribute(DELETED_FROM, session.getAttribute(DELETED_FROM));
            session.removeAttribute(DELETED_GOODS);
            session.removeAttribute(DELETED_FROM);
        }
    }

}

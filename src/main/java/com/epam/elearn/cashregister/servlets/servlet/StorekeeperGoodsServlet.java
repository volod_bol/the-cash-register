package com.epam.elearn.cashregister.servlets.servlet;

import com.epam.elearn.cashregister.database.ConnectionPoolPostgres;
import com.epam.elearn.cashregister.database.DBManager;
import com.epam.elearn.cashregister.entity.Goods;
import com.epam.elearn.cashregister.util.Verifier;
import com.epam.elearn.cashregister.util.ServletHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class StorekeeperGoodsServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(StorekeeperGoodsServlet.class);

    private static final String WRONG_GOODS_INPUT = "wrongGoodsInput";
    private static final String LAST_GOODS_STOREKEEPER = "lastGoodsStorekeeper";
    private static final String CHANGED_QUANTITY_STATE = "changedQuantity";
    private static final String CHANGED_GOODS_ID = "changedGoodsId";
    private static final String CHANGED_GOODS_TITLE = "changedGoodsTitle";
    private static final String NEW_QUANTITY = "newQuantity";

    private final ServletHelper servletHelper = new ServletHelper();
    private final Verifier verifier = new Verifier();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        servletHelper.renderGoods(request);
        servletHelper.markIfWrongInput(request, WRONG_GOODS_INPUT);

        showChanges(request, request.getSession());

        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/WEB-INF/view/roles/storekeeper/storekeeperMain.jsp");
        requestDispatcher.forward(request, response);
    }

    /**
     * Show changed quantity of goods
     *
     * @param request request
     * @param session session
     */
    private void showChanges(HttpServletRequest request, HttpSession session) {
        Goods goods = (Goods) session.getAttribute(LAST_GOODS_STOREKEEPER);
        if (goods != null) {
            request.setAttribute(LAST_GOODS_STOREKEEPER, goods);
            session.removeAttribute(LAST_GOODS_STOREKEEPER);
        }

        Object changedQuantity = session.getAttribute(CHANGED_QUANTITY_STATE);
        if (changedQuantity != null) {
            request.setAttribute(CHANGED_QUANTITY_STATE, changedQuantity);
            request.setAttribute(CHANGED_GOODS_ID, session.getAttribute(CHANGED_GOODS_ID));
            request.setAttribute(CHANGED_GOODS_TITLE, session.getAttribute(CHANGED_GOODS_TITLE));
            request.setAttribute(NEW_QUANTITY, session.getAttribute(NEW_QUANTITY));

            session.removeAttribute(CHANGED_QUANTITY_STATE);
            session.removeAttribute(CHANGED_GOODS_ID);
            session.removeAttribute(CHANGED_GOODS_TITLE);
            session.removeAttribute(NEW_QUANTITY);
        }
    }

    /**
     * Serve post http method on storekeeper page. Logic depends on request parameter,
     * can add new goods or change goods quantity
     *
     * @param request  request
     * @param response response
     * @throws ServletException ServletException
     * @throws IOException      IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "addGoods":
                addNewGoods(request);
                break;
            case "changeGoods":
                changeGoodsQuantity(request);
                break;
            default:
                LOGGER.error(() -> String.format("Cannot recognize action (storekeeper), action: %s", action));
        }
        response.sendRedirect(request.getContextPath() + "/storekeeper");
    }

    /**
     * Add new goods to database
     *
     * @param request request
     */
    private void addNewGoods(HttpServletRequest request) {
        String title = request.getParameter("title");
        String quantity = request.getParameter("quantity");
        if (verifier.isGoodsOrderCorrect(title, quantity)) {
            String quantityInput = quantity.replace(",", ".");

            Connection connection = ((ConnectionPoolPostgres) getServletContext().getAttribute("connectionPool")).getConnection();
            Goods goods = DBManager.insertIntoGoods(connection, title, Double.parseDouble(quantityInput));
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error("Cannot close connection", e);
            }

            addOnPageLastAddedGoodsInfo(request, title, quantityInput, goods);

        } else {
            request.getSession().setAttribute(WRONG_GOODS_INPUT, true);
            LOGGER.error(() -> String.format("Wrong input - cannot add goods to table, title: %s, quantity: %s", title, quantity));
        }
    }

    /**
     * Add information about last added goods on page
     *
     * @param request  request
     * @param title    goods title
     * @param quantity goods quantity
     * @param goods    goods object
     */
    private void addOnPageLastAddedGoodsInfo(HttpServletRequest request, String title, String quantity, Goods goods) {
        if (goods != null) {
            request.getSession().setAttribute(LAST_GOODS_STOREKEEPER, goods);
            LOGGER.info(() -> String.format("Added new goods to table, id: %s, title: %s, quantity: %s", goods.getId(), goods.getTitle(), goods.getQuantity()));
        } else {
            request.getSession().setAttribute(WRONG_GOODS_INPUT, true);
            LOGGER.error(() -> String.format("Cannot add goods to table, title: %s, quantity: %s", title, quantity));
        }
    }

    /**
     * Change quantity of goods
     *
     * @param request request
     */
    private void changeGoodsQuantity(HttpServletRequest request) {
        String goodsId = request.getParameter("goodsId");
        String quantityChange = request.getParameter("quantityChange");

        if (verifier.isOrderInputCorrect("id", goodsId, quantityChange)) {
            addOnPageChangedQuantityInfo(request, goodsId, quantityChange);
        } else {
            request.getSession().setAttribute(WRONG_GOODS_INPUT, true);
            LOGGER.error(() -> String.format("Wrong input, cannot change amount, goodsId: %s, quantityChange: %s", goodsId, quantityChange));
        }
    }

    /**
     * Add information about changed quantity in session and update goods quantity in database
     *
     * @param request        request
     * @param goodsId        goods id
     * @param quantityChange new goods quantity
     */
    private void addOnPageChangedQuantityInfo(HttpServletRequest request, String goodsId, String quantityChange) {
        double quantityDouble = Double.parseDouble(quantityChange.replace(",", "."));
        Connection connection = ((ConnectionPoolPostgres) getServletContext().getAttribute("connectionPool")).getConnection();
        if (DBManager.updateGoodsQuantity(connection, Integer.parseInt(goodsId), quantityDouble)) {
            String title = DBManager.getGoodsOrderById(connection, Integer.parseInt(goodsId), quantityChange).getTitle();
            request.getSession().setAttribute(CHANGED_QUANTITY_STATE, true);
            request.getSession().setAttribute(CHANGED_GOODS_ID, goodsId);
            request.getSession().setAttribute(CHANGED_GOODS_TITLE, title);
            request.getSession().setAttribute(NEW_QUANTITY, quantityDouble);
            LOGGER.info(() -> String.format("Changed quantity of goods, id: %s, title: %s, new quantity: %s", goodsId, title, quantityDouble));
        } else {
            request.getSession().setAttribute(WRONG_GOODS_INPUT, true);
            LOGGER.error(() -> String.format("Cannot change quantity of goods, id: %s, quantityChange: %s", goodsId, quantityChange));
        }
        try {
            connection.close();
        } catch (SQLException e) {
            LOGGER.error("Cannot close connection", e);
        }
    }

}

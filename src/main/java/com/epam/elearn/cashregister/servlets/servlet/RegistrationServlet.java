package com.epam.elearn.cashregister.servlets.servlet;

import com.epam.elearn.cashregister.database.ConnectionPoolPostgres;
import com.epam.elearn.cashregister.database.DBManager;
import com.epam.elearn.cashregister.entity.Role;
import com.epam.elearn.cashregister.entity.User;
import com.epam.elearn.cashregister.util.PasswordEncryptor;
import com.epam.elearn.cashregister.util.ServletHelper;
import com.epam.elearn.cashregister.util.Verifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class RegistrationServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(RegistrationServlet.class);

    private static final String INCORRECT_LOGIN = "incorrectLogin";
    private static final String SUCCESS_REGISTRATION = "successRegistration";
    private static final String USER_LOGIN = "userLogin";
    private static final String USER_PASSWORD = "userPassword";
    private static final String LOGIN_INPUT = "loginInput";
    private static final String USER_ROLE = "userRole";

    private final ServletHelper servletHelper = new ServletHelper();
    private final Verifier verifier = new Verifier();

    /**
     * Show page of registering new user. Show errors, success registration, new user login, password, role.
     *
     * @param request  request
     * @param response response
     * @throws ServletException ServletException
     * @throws IOException      IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        servletHelper.markIfWrongInput(request, INCORRECT_LOGIN);
        servletHelper.markIfWrongInput(request, "isLoginUsed");
        servletHelper.markIfWrongInput(request, "incorrectPassword");
        servletHelper.markIfWrongInput(request, "incorrectRepeatedPassword");

        passParameters(request);

        RequestDispatcher requestDispatcher = getServletContext()
                .getRequestDispatcher("/WEB-INF/view/registration.jsp");
        Object isRegistered = request.getSession().getAttribute(SUCCESS_REGISTRATION);
        if (isRegistered != null) {
            response.sendRedirect(request.getContextPath() + "/login");
        } else {
            requestDispatcher.forward(request, response);
        }
    }

    /**
     * Registering new user in system. Save information about wrong input in session
     *
     * @param request  request
     * @param response response
     * @throws IOException IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Connection connection = ((ConnectionPoolPostgres) getServletContext().getAttribute("connectionPool")).getConnection();

        boolean isInputCorrect = true;
        String userLogin = request.getParameter(USER_LOGIN);
        if (!verifier.isLoginCorrect(userLogin)) {
            isInputCorrect = false;
            request.getSession().setAttribute(INCORRECT_LOGIN, true);
            LOGGER.error(() -> "Cannot register new user, wrong login: " + userLogin);
        }
        if (isInputCorrect && !DBManager.isUserLoginFree(connection, userLogin)) {
            isInputCorrect = false;
            request.getSession().setAttribute("isLoginUsed", true);
            LOGGER.error(() -> "Cannot register new user, login is exist: " + userLogin);
        }

        String userPassword = request.getParameter(USER_PASSWORD);
        if (!verifier.isPasswordCorrect(userPassword)) {
            if (isInputCorrect) {
                isInputCorrect = false;
                request.getSession().setAttribute(LOGIN_INPUT, userLogin);
                LOGGER.error(() -> String.format("Cannot register new user, wrong password: %s, but login correct: %s", userPassword, userLogin));
            }
            request.getSession().setAttribute("incorrectPassword", true);
            LOGGER.error(() -> "Cannot register new user, wrong password: " + userPassword);
        }

        String repeatedUserPassword = request.getParameter("repeatedUserPassword");
        if (!userPassword.equals(repeatedUserPassword)) {
            isInputCorrect = false;
            request.getSession().setAttribute(LOGIN_INPUT, userLogin);
            request.getSession().setAttribute("incorrectRepeatedPassword", true);
            LOGGER.error(() -> String.format("Cannot register new user, password and repeated password are different, password: %s, repeated password: %s", userPassword, repeatedUserPassword));
        }

        saveUserIfInputCorrect(request, connection, isInputCorrect, userLogin, userPassword);

        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        response.sendRedirect(request.getContextPath() + "/sign-up");
    }

    /**
     * Save user with encrypted password in database. Add information about registered user in session
     *
     * @param request        request
     * @param connection     connection to database
     * @param isInputCorrect is all input correct
     * @param userLogin      new user login
     * @param userPassword   new user password
     */
    private void saveUserIfInputCorrect(HttpServletRequest request, Connection connection, boolean isInputCorrect, String userLogin, String userPassword) {
        if (isInputCorrect) {
            PasswordEncryptor passwordEncryptor = (PasswordEncryptor) request.getServletContext().getAttribute("passwordUtils");
            String userRole = request.getParameter(USER_ROLE);
            User newUser = new User(0, userLogin, passwordEncryptor.encryptPassword(userPassword), Role.valueOf(userRole));
            DBManager.addUser(connection, newUser);
            request.getSession().setAttribute(SUCCESS_REGISTRATION, true);
        }
    }

    /**
     * When user registering new user, and put in correct username (login) but
     * wrong password, that method pass correct username on page
     *
     * @param request request
     */
    private void passParameters(HttpServletRequest request) {
        Object isIncorrectLogin = request.getAttribute(INCORRECT_LOGIN);
        Object userLogin = request.getSession().getAttribute(LOGIN_INPUT);
        Object userRole = request.getSession().getAttribute(USER_ROLE);
        if (isIncorrectLogin != null && userLogin != null && userRole != null) {
            request.setAttribute(LOGIN_INPUT, userLogin);
            request.setAttribute(USER_ROLE, userRole);
            request.getSession().removeAttribute(LOGIN_INPUT);
            request.getSession().removeAttribute(USER_ROLE);
        }
    }

}

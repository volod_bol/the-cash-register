package com.epam.elearn.cashregister.servlets.servlet;

import com.epam.elearn.cashregister.database.ConnectionPoolPostgres;
import com.epam.elearn.cashregister.database.DBManager;
import com.epam.elearn.cashregister.entity.GoodsOrder;
import com.epam.elearn.cashregister.util.Verifier;
import com.epam.elearn.cashregister.util.ServletHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

public class CashierCreateOrderServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(CashierCreateOrderServlet.class);

    private static final String ORDER_LIST = "goodsOrderList";
    private static final String INCORRECT_INPUT = "incorrectInputCreateOrder";
    private static final String LAST_ADDED_GOODS = "lastGoods";

    private final ServletHelper servletHelper = new ServletHelper();
    private final Verifier verifier = new Verifier();

    /**
     * Serve get http method on cashier main page. Show create order page.
     *
     * @param request  request
     * @param response response
     * @throws ServletException ServletException
     * @throws IOException      IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        servletHelper.markIfWrongInput(request, INCORRECT_INPUT);
        servletHelper.setOrdersFromSessionToRequest(request, ORDER_LIST);

        Object lastGoods = request.getSession().getAttribute(LAST_ADDED_GOODS);
        request.setAttribute(LAST_ADDED_GOODS, Objects.requireNonNullElseGet(lastGoods, GoodsOrder::new));

        RequestDispatcher requestDispatcher = getServletContext()
                .getRequestDispatcher("/WEB-INF/view/roles/cashier/createOrder.jsp");
        requestDispatcher.forward(request, response);
    }

    /**
     * Serve post http method on cashier main page. Add goods to order.
     *
     * @param request  request
     * @param response response
     * @throws IOException IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        List<GoodsOrder> goodsOrderList = (List<GoodsOrder>) request.getSession().getAttribute(ORDER_LIST);

        String goodsIdentifier = request.getParameter("goodsIdentifier");
        String goodsIdentifierInput = request.getParameter("goodsIdentifierInput");
        String goodsAmount = request.getParameter("goodsAmount");

        if (verifier.isOrderInputCorrect(goodsIdentifier, goodsIdentifierInput, goodsAmount)) {
            addGoodsToOrder(request,
                    goodsOrderList,
                    goodsIdentifier,
                    goodsIdentifierInput,
                    goodsAmount);
        } else {
            request.getSession().setAttribute(INCORRECT_INPUT, true);
            LOGGER.error(() -> String.format("Invalid input for goodsOrder item: goodsIdentifierInput: %s, goodsAmount: %s", goodsIdentifierInput, goodsAmount));
        }

        response.sendRedirect(request.getContextPath() + "/cashier/create-order");
    }

    /**
     * Add goods to order. If goods don't exist or input incorrect add to request information about it
     * and show it with get http method.
     *
     * @param request              request
     * @param goodsOrderList       list with goods, receipt
     * @param goodsIdentifier      identifier of goods, id or title
     * @param goodsIdentifierInput identifier input
     * @param goodsAmount          amount of goods
     */
    private void addGoodsToOrder(HttpServletRequest request,
                                 List<GoodsOrder> goodsOrderList,
                                 String goodsIdentifier,
                                 String goodsIdentifierInput,
                                 String goodsAmount) {
        GoodsOrder goodsOrder = getGoodsOrder(goodsIdentifier, goodsIdentifierInput, goodsAmount.replace(",", "."));
        if (goodsOrder != null) {
            addGoodsToList(goodsOrderList, goodsOrder);
            request.getSession().setAttribute(LAST_ADDED_GOODS, goodsOrder);
            String goodsOrderInfo = goodsOrder.toString();
            LOGGER.info(() -> String.format("ID (Title): %s Amount: %s goodsOrder: %s", goodsIdentifierInput, goodsAmount, goodsOrderInfo));
        } else {
            request.getSession().setAttribute(INCORRECT_INPUT, true);
            LOGGER.info(() -> String.format("ID (Title): %s Amount: %s goodsOrder: null (incorrect input)", goodsIdentifierInput, goodsAmount));
        }
    }

    /**
     * Trying to find goods by identifier in database.
     *
     * @param goodsIdentifier      identifier of goods, id or title
     * @param goodsIdentifierInput identifier input
     * @param goodsAmount          amount of goods
     * @return goods for order with amount
     */
    private GoodsOrder getGoodsOrder(String goodsIdentifier, String goodsIdentifierInput, String goodsAmount) {
        Connection connection = ((ConnectionPoolPostgres) getServletContext().getAttribute("connectionPool")).getConnection();

        GoodsOrder goodsOrder = null;
        switch (goodsIdentifier) {
            case "id":
                goodsOrder = DBManager.getGoodsOrderById(connection, Integer.parseInt(goodsIdentifierInput), goodsAmount);
                break;
            case "title":
                goodsOrder = DBManager.getGoodsByTitle(connection, goodsIdentifierInput, goodsAmount);
                break;
            default:
                LOGGER.error(() -> "Can not recognize goodsIdentifier");
        }

        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return goodsOrder;
    }

    /**
     * Add goods to order list. If that goods already was added, then just sum their amount
     *
     * @param goodsOrderList order list with goods
     * @param item           new goods that require adding to order
     */
    private void addGoodsToList(List<GoodsOrder> goodsOrderList, GoodsOrder item) {
        goodsOrderList.stream()
                .filter(goodsOrder -> goodsOrder.getTitle().equals(item.getTitle()))
                .findFirst()
                .ifPresentOrElse(
                        goodsOrder -> {
                            double currAmount = Double.parseDouble(goodsOrder.getAmount());
                            double newAmount = Double.parseDouble(item.getAmount());
                            goodsOrder.setAmount(String.valueOf(currAmount + newAmount));
                        },
                        () -> goodsOrderList.add(item)
                );
    }

}

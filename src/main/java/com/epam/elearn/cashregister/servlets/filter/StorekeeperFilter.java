package com.epam.elearn.cashregister.servlets.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * If authorized user hasn't STOREKEEPER role then will get 401 error.
 * ALso filter set character encoding to UTF-8
 */
public class StorekeeperFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        req.setCharacterEncoding("UTF-8");

        String role = (String) req.getSession().getAttribute("role");
        if (role != null && role.equals("STOREKEEPER")) {
            chain.doFilter(request, response);
        } else {
            res.sendError(401);
        }
    }
}

package com.epam.elearn.cashregister.servlets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * Custom tag for jsp pages. Recognize raw role and print correct role on
 * page depends on language
 */
public class RecognizeRole extends SimpleTagSupport {

    private static final Logger LOGGER = LogManager.getLogger(RecognizeRole.class);

    private String role;
    private String language;

    public void setRole(String role) {
        this.role = role;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public void doTag() throws JspException, IOException {
        if (language.equals("ua")) {
            switch (role) {
                case "CASHIER":
                    getJspContext().getOut().write("Касир");
                    break;
                case "STOREKEEPER":
                    getJspContext().getOut().write("Товарознавець");
                    break;
                case "SENIOR_CASHIER":
                    getJspContext().getOut().write("Старший касир");
                    break;
                default:
                    LOGGER.error(() -> "Cannot recognize role: " + role);
            }
        } else if (language.equals("en")) {
            switch (role) {
                case "CASHIER":
                    getJspContext().getOut().write("Cashier");
                    break;
                case "STOREKEEPER":
                    getJspContext().getOut().write("Storekeeper");
                    break;
                case "SENIOR_CASHIER":
                    getJspContext().getOut().write("Senior Cashier");
                    break;
                default:
                    LOGGER.error(() -> "Cannot recognize role: " + role);
            }
        }
    }
}

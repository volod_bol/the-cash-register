package com.epam.elearn.cashregister.database;

import com.epam.elearn.cashregister.entity.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class DBManager {
    private static final Logger LOGGER = LogManager.getLogger(DBManager.class);

    private static final String QUANTITY = "quantity";
    private static final String TITLE = "title";
    private static final String USER_LOGIN = "user_login";

    DBManager() {
        throw new IllegalStateException();
    }

    /**
     * Trying to find user in database. If user don't exist or happened error
     * during query returning null.
     *
     * @param connection connection to database
     * @param login      user login
     * @return found user in database
     */
    public static User getUser(Connection connection, String login) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.GET_USER_BY_LOGIN.query)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                String password = resultSet.getString("password");
                Role role = Role.valueOf(resultSet.getString("role"));
                return new User(id, login, password, role);
            }
        } catch (SQLException e) {
            LOGGER.error(String.format("Can not get user from db, login: '%s'", login), e);
            return null;
        }
        return null;
    }

    /**
     * Trying to add new user to database. If during query happened error return current user without
     * id field
     *
     * @param connection connection to database
     * @param user       user object
     * @return added user to database with id field
     */
    public static User addUser(Connection connection, User user) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.INSERT_INTO_USERS.query)) {
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getRole().toString());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
            }
        } catch (SQLException e) {
            LOGGER.error(() -> String.format("Cannot add new user to users. Login: %s, password: %s, role: %s", user.getLogin(), user.getPassword(), user.getRole().toString()), e);
            return user;
        }
        return user;
    }

    /**
     * Checking that user login is free. If login exist return false.
     *
     * @param connection connection to database
     * @param userLogin  user login
     * @return true if free and false if login exist
     */
    public static boolean isUserLoginFree(Connection connection, String userLogin) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.IS_LOGIN_EXIST.query)) {
            preparedStatement.setString(1, userLogin);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return false;
            }
        } catch (SQLException e) {
            LOGGER.error(() -> String.format("Cannot execute query and get user id, userLogin: %s", userLogin), e);
            return false;
        }
        return true;
    }

    /**
     * Return count of goods in database. If during query happened error will be returned zero.
     *
     * @param connection connection to database
     * @return count of goods in database
     */
    public static int getCountOfGoods(Connection connection) {
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT count(id) FROM goods");
            if (resultSet.next()) {
                return resultSet.getInt("count");
            }
        } catch (SQLException e) {
            LOGGER.error("Cannot count quantity of goods", e);
            return 0;
        }
        return 0;
    }

    /**
     * Return count of orders in database. If during query happened error will be returned zero.
     *
     * @param connection connection to database
     * @return count of orders in database
     */
    public static int getCountOfOrders(Connection connection) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.GET_COUNT_OF_ORDERS.query)) {
            preparedStatement.setInt(1, getLastOrderReportId(connection));
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("count");
            }
        } catch (SQLException e) {
            LOGGER.error("Cannot count quantity of orders", e);
            return 0;
        }
        return 0;
    }

    /**
     * Trying to add new goods in database. If during query happened error will be returned null
     *
     * @param connection connection to database
     * @param title      goods title
     * @param quantity   goods quantity
     * @return goods with id field or due to an error null
     */
    public static Goods insertIntoGoods(Connection connection, String title, double quantity) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.INSERT_INTO_GOODS.query)) {
            preparedStatement.setString(1, title);
            preparedStatement.setDouble(2, quantity);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return new Goods(resultSet.getInt("id"), title, quantity);
            }
        } catch (SQLException e) {
            LOGGER.error(String.format("Cannot insert into goods table, title: %s, quantity: %s", title, quantity), e);
            return null;
        }
        return null;
    }

    /**
     * Trying to change goods quantity. If id greater than all numbers of goods or during
     * query happened error will be returned false.
     *
     * @param connection connection to database
     * @param id         goods id
     * @param quantity   goods new quantity
     * @return true if changed or due to an error false
     */
    public static boolean updateGoodsQuantity(Connection connection, int id, double quantity) {
        int allGoods = getCountOfGoods(connection);
        if (id > allGoods)
            return false;
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.UPDATE_QUANTITY_OF_GOODS.query)) {
            preparedStatement.setDouble(1, quantity);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            LOGGER.error(String.format("Error during update goods query, id: %s, quantity: %s", id, quantity), e);
            return false;
        }
    }

    /**
     * Return all goods from database according to offset and limit.
     *
     * @param connection connection to database
     * @param limit      limit of items
     * @param offset     offset of items
     * @return list of goods
     */
    public static List<Goods> getAllGoods(Connection connection, int limit, int offset) {
        List<Goods> goodsList = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.GET_ALL_GOODS_PAGINATION.query)) {
            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String title = resultSet.getString(TITLE);
                double quantity = resultSet.getDouble(QUANTITY);
                goodsList.add(new Goods(id, title, quantity));
            }
            return goodsList;
        } catch (SQLException e) {
            LOGGER.error(String.format("Cannot get all goods (pagination) limit: %s, offset: %s", limit, offset), e);
            return goodsList;
        }
    }

    /**
     * Trying to find order. If during query happened error will be returned null.
     *
     * @param connection connection to database
     * @param id         order id
     * @return order object or due to an error null
     */
    public static Order getOrder(Connection connection, int id) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.GET_ORDER_BY_ID.query)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String userLogin = resultSet.getString(USER_LOGIN);
                LocalDate date = resultSet.getDate("date").toLocalDate();
                LocalTime time = resultSet.getTime("time").toLocalTime();
                return new Order(id, userLogin, new ArrayList<>(), date, time);
            }
        } catch (SQLException e) {
            LOGGER.error(() -> "Cannot get order, id: " + id);
            return null;
        }
        return null;
    }

    /**
     * Return all orders according to last order id in Z report.
     *
     * @param connection connection to database
     * @return list of orders
     */
    public static List<Order> getAllOrders(Connection connection) {
        List<Order> orderList = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.GET_ALL_ORDERS.query)) {
            preparedStatement.setInt(1, getLastOrderReportId(connection));
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String userLogin = resultSet.getString(USER_LOGIN);
                List<GoodsOrder> goodsFromOrder = getAllGoodsFromOrder(connection, id);
                LocalDate date = resultSet.getDate("date").toLocalDate();
                LocalTime time = resultSet.getTime("time").toLocalTime();
                orderList.add(new Order(id, userLogin, goodsFromOrder, date, time));
            }
            return orderList;
        } catch (SQLException e) {
            LOGGER.error("Cannot get all orders", e);
            return orderList;
        }
    }

    /**
     * Return all orders from database according to offset and limit.
     *
     * @param connection connection to database
     * @param limit      limit of items
     * @param offset     offset of items
     * @return list of orders
     */
    public static List<Order> getAllOrders(Connection connection, int limit, int offset) {
        List<Order> orderList = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.GET_ALL_ORDERS_PAGINATION.query)) {
            preparedStatement.setInt(1, getLastOrderReportId(connection));
            preparedStatement.setInt(2, limit);
            preparedStatement.setInt(3, offset);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String userLogin = resultSet.getString(USER_LOGIN);
                List<GoodsOrder> goodsFromOrder = getAllGoodsFromOrder(connection, id);
                LocalDate date = resultSet.getDate("date").toLocalDate();
                LocalTime time = resultSet.getTime("time").toLocalTime();
                orderList.add(new Order(id, userLogin, goodsFromOrder, date, time));
            }
            return orderList;
        } catch (SQLException e) {
            LOGGER.error(String.format("Cannot get all orders (pagination) limit: %s, offset: %s", limit, offset), e);
            return orderList;
        }
    }

    /**
     * Trying to delete order. If order don't exist or during query happened error
     * will be returned null.
     *
     * @param connection connection to database
     * @param id         order id
     * @return deleted order object
     */
    public static Order deleteOrder(Connection connection, int id) {
        Order order = getOrder(connection, id);
        if (order == null) {
            return null;
        }
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.DELETE_ORDER.query)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            return order;
        } catch (SQLException e) {
            LOGGER.error(String.format("Cannot delete order, id: %s", id), e);
            return null;
        }
    }

    /**
     * Trying to delete goods in order. If during query happened error will be returned null.
     * If after deleting goods order will be empty, it will be deleted too.
     *
     * @param connection connection to database
     * @param orderId    order id
     * @param goodsId    goods id
     * @return deleted goods from order or due to an error null
     */
    public static GoodsOrder deleteGoodsInOrder(Connection connection, int orderId, int goodsId) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.DELETE_GOODS_IN_ORDER.query)) {
            preparedStatement.setInt(1, orderId);
            preparedStatement.setInt(2, goodsId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                if (getAllGoodsFromOrder(connection, orderId).isEmpty()) {
                    deleteOrder(connection, orderId);
                }
                return getGoodsOrderById(connection, goodsId, String.valueOf(resultSet.getDouble("goods_amount")));
            }
        } catch (SQLException e) {
            LOGGER.error(String.format("Cannot delete goods from order, orderId: %s, goodsId: %s", orderId, goodsId), e);
            return null;
        }
        return null;
    }

    /**
     * Trying to find goods by id and map it to goods from order (additional field amount).
     * If goods don't exist or during query happened error will be returned null.
     *
     * @param connection connection to database
     * @param id         goods id
     * @param amount     goods amount in order
     * @return goods from order with amount
     */
    public static GoodsOrder getGoodsOrderById(Connection connection, int id, String amount) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.GET_GOODS_BY_ID.query)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return new GoodsOrder(id, resultSet.getString(TITLE), amount);
            }
        } catch (SQLException e) {
            LOGGER.error(String.format("Can execute query in getGoodsById, id: %s", id), e);
            return null;
        }
        return null;
    }

    /**
     * Trying to find goods by title and map it to goods from order (additional field amount).
     * If goods don't exist or during query happened error will be returned null.
     *
     * @param connection connection to database
     * @param title      goods title
     * @param amount     goods amount
     * @return goods from order with amount
     */
    public static GoodsOrder getGoodsByTitle(Connection connection, String title, String amount) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.GET_GOODS_BY_TITLE.query)) {
            preparedStatement.setString(1, title);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return new GoodsOrder(resultSet.getInt("id"), title, amount);
            }
        } catch (SQLException e) {
            LOGGER.error(String.format("Can execute query in getGoodsByTitle, title: %s", title), e);
            return null;
        }
        return null;
    }

    /**
     * Return all goods with amount from order.
     *
     * @param connection connection to database
     * @param id         order id
     * @return list of goods from order
     */
    static List<GoodsOrder> getAllGoodsFromOrder(Connection connection, int id) {
        List<GoodsOrder> goodsOrderList = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.GET_ALL_GOODS_IN_ORDER.query)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int goodsId = resultSet.getInt("goods_id");
                String title = resultSet.getString(TITLE);
                double goodsAmount = resultSet.getDouble("goods_amount");
                goodsOrderList.add(new GoodsOrder(goodsId, title, String.valueOf(goodsAmount)));
            }
        } catch (SQLException e) {
            LOGGER.error(String.format("Cannot get all goods from order, order id: %s", id), e);
            return goodsOrderList;
        }
        return goodsOrderList;
    }

    /**
     * Save order to database and all goods from order to intermediate table.
     * It's transaction method.
     *
     * @param connection     connection to database
     * @param goodsOrderList list with goods in order
     * @param userLogin      cashier login
     * @return true if saved or false if no
     */
    public static boolean confirmOrder(Connection connection, List<GoodsOrder> goodsOrderList, String userLogin) {
        turnOffAutoCommit(connection);
        try (PreparedStatement preparedStatementOrders = connection.prepareStatement(SQLQuery.INSERT_INTO_ORDERS.query);
             PreparedStatement preparedStatementDetail = connection.prepareStatement(SQLQuery.INSERT_INTO_ORDERS_DETAIL.query)) {

            preparedStatementOrders.setString(1, userLogin);
            ResultSet resultSet = preparedStatementOrders.executeQuery();
            int orderId = 0;
            if (resultSet.next()) {
                orderId = resultSet.getInt("id");
            }

            for (GoodsOrder goods : goodsOrderList) {
                preparedStatementDetail.setInt(1, orderId);
                preparedStatementDetail.setInt(2, goods.getId());
                preparedStatementDetail.setDouble(3, Double.parseDouble(goods.getAmount().replace(",", ".")));
                preparedStatementDetail.executeUpdate();
            }
            connection.commit();
        } catch (SQLException e) {
            rollbackChanges(connection);
            turnOnAutoCommit(connection);
            LOGGER.error("Error during confirming order", e);
            return false;
        }
        turnOnAutoCommit(connection);
        return true;
    }

    /**
     * Return count of purchased goods from last Z report.
     *
     * @param connection connection to database
     * @return count of purchased goods
     */
    public static int getCountPurchasedGoods(Connection connection) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.GET_COUNT_PURCHASED_GOODS.query)) {
            preparedStatement.setInt(1, getLastOrderReportId(connection));
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("sum");
            }
        } catch (SQLException e) {
            LOGGER.error("Cannot get count of purchased goods", e);
            return 0;
        }
        return 0;
    }

    /**
     * Confirm X report. Save information about last order id from Z report and
     * cashier login.
     *
     * @param connection connection to database
     * @param userLogin  cashier login
     * @return report id
     */
    public static int confirmXReport(Connection connection, String userLogin) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.CONFIRM_X_REPORT.query)) {
            preparedStatement.setInt(1, getLastOrderReportId(connection));
            preparedStatement.setString(2, userLogin);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("id");
            }
        } catch (SQLException e) {
            LOGGER.error("Cannot confirm X report", e);
            return 0;
        }
        return 0;
    }

    /**
     * Confirm Z report. Save information about last order id and cashier login.
     *
     * @param connection connection to database
     * @param userLogin  cashier login
     * @return report id
     */
    public static int confirmZReport(Connection connection, String userLogin) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLQuery.CONFIRM_Z_REPORT.query)) {
            preparedStatement.setInt(1, getLastOrderId(connection));
            preparedStatement.setString(2, userLogin);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("id");
            }
        } catch (SQLException e) {
            LOGGER.error("Cannot confirm Z report", e);
            return 0;
        }
        return 0;
    }

    /**
     * Get last order id.
     *
     * @param connection connection to database
     * @return last order id
     */
    static int getLastOrderId(Connection connection) {
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT id FROM orders ORDER BY id DESC LIMIT 1");
            if (resultSet.next()) {
                return resultSet.getInt("id");
            }
        } catch (SQLException e) {
            LOGGER.error("Cannot get last order id", e);
            return 0;
        }
        return 0;
    }

    /**
     * Get last order id from last Z report.
     *
     * @param connection connection to database
     * @return last order id
     */
    static int getLastOrderReportId(Connection connection) {
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT last_orders_id FROM report_dates WHERE type_of_report = 'Z' ORDER BY id DESC LIMIT 1");
            if (resultSet.next()) {
                return resultSet.getInt("last_orders_id");
            }
        } catch (SQLException e) {
            LOGGER.error("Cannot get last order report id", e);
            return 0;
        }
        return 0;
    }

    /**
     * Turn on auto commit
     *
     * @param connection connection to database
     */
    private static void turnOnAutoCommit(Connection connection) {
        try {
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Turn off auto commit
     *
     * @param connection connection to database
     */
    private static void turnOffAutoCommit(Connection connection) {
        try {
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Rollback last changes in database
     *
     * @param connection connection to database
     */
    private static void rollbackChanges(Connection connection) {
        try {
            connection.rollback();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Enum with prepared queries
     */
    enum SQLQuery {
        GET_USER_BY_LOGIN("SELECT * FROM users WHERE login = ?"),
        IS_LOGIN_EXIST("SELECT id FROM users WHERE login = ?"),
        INSERT_INTO_USERS("INSERT INTO users (id, login, password, role) VALUES (DEFAULT, ?, ?, ?) RETURNING id"),
        GET_ALL_GOODS("SELECT * FROM goods ORDER BY title"),
        GET_ALL_GOODS_PAGINATION("SELECT * FROM goods ORDER BY title LIMIT ? OFFSET ?"),
        GET_ORDER_BY_ID("SELECT * FROM orders WHERE id = ?"),
        GET_COUNT_OF_ORDERS("SELECT count(id) FROM orders WHERE id > ?"),
        GET_ALL_ORDERS("SELECT * FROM orders WHERE id > ? ORDER BY id DESC"),
        GET_ALL_ORDERS_PAGINATION("SELECT * FROM orders WHERE id > ? ORDER BY id DESC LIMIT ? OFFSET ?"),
        GET_COUNT_PURCHASED_GOODS("SELECT sum(goods_amount)" +
                " FROM (orders_detail INNER JOIN goods g on g.id = orders_detail.goods_id" +
                " INNER JOIN orders o on o.id = orders_detail.orders_id) WHERE orders_id > ?"),
        DELETE_GOODS_IN_ORDER("DELETE FROM orders_detail WHERE orders_id = ? AND goods_id = ? RETURNING goods_amount"),
        GET_ALL_GOODS_IN_ORDER("SELECT orders_detail.goods_id, title, orders_detail.goods_amount" +
                " FROM (orders_detail INNER JOIN goods g on g.id = orders_detail.goods_id)" +
                " INNER JOIN orders o on o.id = orders_detail.orders_id WHERE orders_id = ?"),
        DELETE_ORDER("DELETE FROM orders WHERE id = ?"),
        GET_GOODS_BY_ID("SELECT * FROM goods WHERE id = ?"),
        GET_GOODS_BY_TITLE("SELECT * FROM goods WHERE title = ?"),
        INSERT_INTO_ORDERS("INSERT INTO orders (id, user_login, date, time) VALUES (DEFAULT, ?, DEFAULT, DEFAULT) RETURNING id"),
        INSERT_INTO_ORDERS_DETAIL("INSERT INTO orders_detail (orders_id, goods_id, goods_amount) VALUES (?, ?, ?)"),
        INSERT_INTO_GOODS("INSERT INTO goods (id, title, quantity) VALUES (DEFAULT, ?, ?) RETURNING id"),
        UPDATE_QUANTITY_OF_GOODS("UPDATE goods SET quantity = ? WHERE id = ?"),
        CONFIRM_X_REPORT("INSERT INTO report_dates (id, last_orders_id, type_of_report, user_login, date, time)" +
                " VALUES (DEFAULT, ?, DEFAULT, ?, DEFAULT, DEFAULT) RETURNING id"),
        CONFIRM_Z_REPORT("INSERT INTO report_dates (id, last_orders_id, type_of_report, user_login, date, time)" +
                " VALUES (DEFAULT, ?, 'Z', ?, DEFAULT, DEFAULT) RETURNING id");

        final String query;

        SQLQuery(String query) {
            this.query = query;
        }
    }
}
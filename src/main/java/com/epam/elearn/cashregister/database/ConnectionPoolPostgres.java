package com.epam.elearn.cashregister.database;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Connection pool to database. Designed by singleton (Bill Pugh singleton).
 * Basic settings: abandoned connections will be removed after 240s. Maximum idle connections are 7.
 * Maximum active connections are 10, before specific test (tests) must be changed.
 */
public class ConnectionPoolPostgres {
    private static final Logger LOGGER = LogManager.getLogger(ConnectionPoolPostgres.class);

    private DataSource dataSource;

    private ConnectionPoolPostgres() {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try (InputStream inputStream = classLoader.getResourceAsStream("app.properties")) {
            Properties properties = getProperties(inputStream);
            String url = properties.getProperty("connection.url");
            String user = properties.getProperty("user");
            String password = properties.getProperty("password");

            PoolProperties poolProperties = new PoolProperties();
            poolProperties.setUrl(url);
            poolProperties.setUsername(user);
            poolProperties.setPassword(password);
            poolProperties.setDriverClassName("org.postgresql.Driver");
            poolProperties.setJmxEnabled(true);
            poolProperties.setTestWhileIdle(false);
            poolProperties.setTestOnBorrow(true);
            poolProperties.setValidationQuery("SELECT 1");
            poolProperties.setTestOnReturn(false);
            poolProperties.setValidationInterval(30000);
            poolProperties.setTimeBetweenEvictionRunsMillis(30000);
            poolProperties.setMaxActive(10);
            poolProperties.setMaxIdle(7);
            poolProperties.setInitialSize(10);
            poolProperties.setMaxWait(10000);
            poolProperties.setRemoveAbandonedTimeout(240);
            poolProperties.setMinEvictableIdleTimeMillis(30000);
            poolProperties.setMinIdle(5);
            poolProperties.setLogAbandoned(true);
            poolProperties.setRemoveAbandoned(true);
            poolProperties.setJdbcInterceptors(
                    "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;" +
                            "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");

            dataSource = new DataSource(poolProperties);
            LOGGER.info(() -> "Connection pool initialized!");
        } catch (IOException e) {
            LOGGER.error(() -> "Connection pool don't initialized!", e);
        }
    }

    private Properties getProperties(InputStream inputStream) {
        Properties properties = new Properties();
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            LOGGER.error("Error during read app.properties", e);
        }
        return properties;
    }

    private static class SingletonHelper {
        private static final ConnectionPoolPostgres INSTANCE = new ConnectionPoolPostgres();
    }

    public static ConnectionPoolPostgres getInstance() {
        return SingletonHelper.INSTANCE;
    }

    /**
     * Get connection form Tomcat connection pool
     *
     * @return connection to database
     */
    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = dataSource.getPool().getConnection();
        } catch (SQLException e) {
            LOGGER.error("Error connecting to database", e);
        }
        return connection;
    }

    public void shutdown() {
        dataSource.close();
    }
}

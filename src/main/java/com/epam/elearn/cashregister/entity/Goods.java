package com.epam.elearn.cashregister.entity;

import java.io.Serializable;

/**
 * Goods from warehouse. Has id, title and quantity
 */
public class Goods implements Serializable {

    private static final long serialVersionUID = 2041243512249239990L;

    private int id;
    private String title;
    private double quantity;

    public Goods() {
        this.id = 0;
        this.title = "";
        this.quantity = 0;
    }

    public Goods(int id, String title, double quantity) {
        this.id = id;
        this.title = title;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Goods goods = (Goods) o;

        return title.equals(goods.title);
    }

    @Override
    public int hashCode() {
        return title.hashCode();
    }

    @Override
    public String toString() {
        return "Goods{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}

package com.epam.elearn.cashregister.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * Goods from warehouse in order. Has id, title and additional field amount instead quantity
 */
public class GoodsOrder implements Serializable {

    private static final long serialVersionUID = 2021243512288239990L;

    private int id;
    private String title;
    private String amount;

    public GoodsOrder() {
        this.id = 0;
        this.title = "";
        this.amount = "";
    }

    public GoodsOrder(int id, String title, String amount) {
        this.id = id;
        this.title = title;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GoodsOrder that = (GoodsOrder) o;
        return id == that.id && title.equals(that.title) && amount.equals(that.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, amount);
    }

    @Override
    public String toString() {
        return "GoodsOrder{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", amount=" + amount +
                '}';
    }
}

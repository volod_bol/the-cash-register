package com.epam.elearn.cashregister.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Order, has id, user login who's created order, list of goods and date and time of creation
 */
public class Order implements Serializable {

    private static final long serialVersionUID = 2055243512249231590L;

    private int id;
    private String userLogin;
    private List<GoodsOrder> goodsList;
    private LocalDate date;
    private LocalTime time;

    public Order() {
        this.id = 0;
        this.userLogin = "";
        goodsList = new ArrayList<>();
        this.date = LocalDate.now();
        this.time = LocalTime.now();
    }

    public Order(int id, String userLogin, List<GoodsOrder> goodsList, LocalDate date, LocalTime time) {
        this.id = id;
        this.userLogin = userLogin;
        this.goodsList = goodsList;
        this.date = date;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public List<GoodsOrder> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<GoodsOrder> goodsList) {
        this.goodsList = goodsList;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return date.equals(order.date) && time.equals(order.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, time);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", userLogin='" + userLogin + '\'' +
                ", date=" + date +
                ", time=" + time +
                '}';
    }
}

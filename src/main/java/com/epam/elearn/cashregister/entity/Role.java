package com.epam.elearn.cashregister.entity;

/**
 * User`s roles
 */
public enum Role {
    CASHIER, SENIOR_CASHIER, STOREKEEPER, UNKNOWN
}
